package day14

import (
	"fmt"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input14.txt")
	initial, rules := parseInput(values)
	executeRules(initial, rules, 10)
	executeRulesPart2(initial, rules, 40)
}

func parseInput(values []string) (initial string, rules map[string]string) {
	initial = values[0]
	rules = make(map[string]string)
	for i := 2; i < len(values); i++ {
		parts := strings.Split(values[i], " -> ")
		rules[parts[0]] = parts[1]
	}
	return
}

func executeRules(initial string, rules map[string]string, runs int) string {
	result := initial
	for r := 0; r < runs; r++ {
		// create a list of insertions to do -> the rule to a list of positions
		insertions := make([]string, len(result))
		for i := 0; i < len(result)-1; i++ {
			for k, v := range rules {
				check := result[i : i+2]
				if check == k {
					insertions[i] = v
				}
			}
		}
		inserted := 0
		for i, insert := range insertions {
			if insert == "" {
				continue
			}
			result = result[0:i+inserted+1] + insert + result[i+inserted+1:]
			inserted++
		}
		// perform the inserts
		// fmt.Printf("\n%02d: %s\n", r, result)
		fmt.Printf("\n%02d\n", r+1)
		countCharacters(result)
	}
	return result
}

func executeRulesPart2(initial string, rules map[string]string, runs int) {
	// convert the initial string into a map of 2-Character counts
	lastChar := initial[len(initial)-1]
	result := countPairs(initial)
	fmt.Printf("\n%d:\n", 0)
	countCharactersFromPairs(result, rune(lastChar))
	for r := 0; r < runs; r++ {
		newResult := make(map[string]int64)
		// for each pair in the result, find the rule that applies.
		// In the new result, add the two new pairs that get created
		for pair, count := range result {
			if _, ok := rules[pair]; !ok {
				insertPair(newResult, pair, count)
				continue
			}
			pair1 := string(pair[0]) + rules[pair]
			pair2 := rules[pair] + string(pair[1])
			insertPair(newResult, pair1, count)
			insertPair(newResult, pair2, count)
			result[pair] = 0
		}
		result = newResult
		fmt.Printf("\n%d:\n", r+1)
		countCharactersFromPairs(result, rune(lastChar))
	}
}

func insertPair(result map[string]int64, pair string, count int64) {
	val, ok := result[pair]
	if !ok {
		result[pair] = count
	} else {
		result[pair] = val + count
	}
}

func countPairs(initial string) map[string]int64 {
	result := make(map[string]int64)
	for i := 0; i < len(initial)-1; i++ {
		pair := initial[i : i+2]
		val, ok := result[pair]
		if !ok {
			result[pair] = 1
		} else {
			result[pair] = val + 1
		}
	}
	return result
}

func countCharactersFromPairs(pairMap map[string]int64, lastExtra rune) {
	result := make(map[rune]int64)
	result[lastExtra] = 1
	for pair, count := range pairMap {
		letter := rune(pair[0])
		// only bother with the first letter
		val, ok := result[letter]
		if !ok {
			result[letter] = count
		} else {
			result[letter] = val + count
		}
	}

	most := int64(0)
	least := result[lastExtra]
	for letter, count := range result {
		fmt.Printf("%v: %d\n", string(letter), count)
		if count > most {
			most = count
		}
		if count < least {
			least = count
		}
	}
	fmt.Printf("\nmost - least: %d - %d = %d\n", most, least, most-least)
}

func countCharacters(str string) {
	most := 0
	least := len(str)
	counts := make(map[rune]int)
	for _, char := range str {
		if _, ok := counts[char]; !ok {
			counts[char] = 0
		}
		counts[char]++
	}
	for k, v := range counts {
		if v > most {
			most = v
		}
		if v < least {
			least = v
		}
		fmt.Printf("%v: %d\n", string(k), v)
	}
	fmt.Printf("\nmost - least: %d - %d = %d\n", most, least, most-least)
}
