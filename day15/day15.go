package day15

import (
	"container/heap"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type Edge struct {
	vertex int
	weight int
}

type Graph map[int][]Edge

func RunDay() {
	values := helpers.ReadFileAsStrings("input15.txt")
	numericValues := multiplyInputSize(values, 5)
	graph := parseGraph(numericValues)
	addEdges(graph, numericValues)
	path := shortestPath(graph, 0, len(graph)-1)
	printPath(numericValues, path)
}

// only works if multi is 5 or less
func multiplyInputSize(rawInput []string, multi int) (output [][]int) {
	output = make([][]int, len(rawInput)*multi)
	for i := 0; i < len(rawInput)*multi; i++ {
		outputRow := make([]int, len(rawInput)*multi)
		output[i] = outputRow
	}
	for row, str := range rawInput {
		for col, char := range str {
			// insert the correct character everywhere in the output
			for r := 0; r < multi; r++ {
				for c := 0; c < multi; c++ {
					addOn := r + c
					value := (int(char-'0') + addOn)
					if value >= 10 { // roll over to 1 so have to do this stupidly
						value -= 9
					}
					// put in correct location
					destRow := (len(rawInput) * r) + row
					destCol := (len(str) * c) + col
					output[destRow][destCol] = value
				}
			}
		}
	}
	return
}

func shortestPath(graph Graph, first int, target int) []int {
	vertexDistances := make([]int, len(graph))
	vertexPredecessors := make(map[int]int, len(graph))
	visited := make(map[int]bool, len(graph))
	pq := make(helpers.PriorityQueue, len(graph))
	for i := 0; i < len(vertexDistances); i++ {
		vertexDistances[i] = math.MaxInt
		vertexPredecessors[i] = -1
		visited[i] = false
		pq[i] = &helpers.PQItem{
			Value:    i,
			Index:    i, // in this case the index is value
			Priority: math.MaxInt,
		}
	}

	vertexDistances[first] = 0
	heap.Init(&pq)
	temp := pq.GetItemByValue(first)
	if temp != nil {
		item, ok := temp.(*helpers.PQItem)
		if !ok {
			log.Fatalln("Oh man, you goofed up bigtime")
		}
		pq.Update(item, first, 0)
	}
	for pq.Len() > 0 {
		var temp interface{}
		temp = heap.Pop(&pq)
		currentV, ok := temp.(*helpers.PQItem)
		if !ok {
			log.Fatalln("Oh man, you goofed up bigtime")
		}

		// Use the graph to BFS traverse the list of vertices adjacent to vertex
		for _, edge := range graph[currentV.Value] {
			// if this vertex is not visited yet
			if visited[edge.vertex] {
				continue
			}
			if vertexDistances[currentV.Value]+edge.weight <
				vertexDistances[edge.vertex] {

				vertexDistances[edge.vertex] =
					vertexDistances[currentV.Value] + edge.weight
				vertexPredecessors[edge.vertex] = currentV.Value
				// Update the priority queue
				temp := pq.GetItemByValue(edge.vertex)
				if temp != nil {
					item, ok := temp.(*helpers.PQItem)
					if !ok {
						log.Fatalln("Oh man, you goofed up bigtime")
					}
					pq.Update(item, edge.vertex, vertexDistances[edge.vertex])
				}
			}
		}

		visited[currentV.Value] = true
		if currentV.Value == target {
			fmt.Printf("Made it to target, distance: %d\n", vertexDistances[currentV.Value])
			break
		}
	}

	// Build Path from Predecessors
	result := []int{}
	current := target
	for true {
		result = append(result, current)
		current = vertexPredecessors[current]
		if current < 0 {
			break
		}
	}
	return result
}

func parseGraph(numericInput [][]int) Graph {
	result := make(Graph)
	vertexIdx := 0
	for row := 0; row < len(numericInput); row++ {
		for col := 0; col < len(numericInput[0]); col++ {
			result[vertexIdx] = make([]Edge, 0)
			vertexIdx++
		}
	}
	return result
}

func addEdges(graph Graph, numericInput [][]int) {
	vertexIdx := 0
	for row := 0; row < len(numericInput); row++ {
		for col := 0; col < len(numericInput[0]); col++ {
			// Add edges to adjacent nodes, where possible
			// above
			if row > 0 {
				graph[vertexIdx] = append(graph[vertexIdx],
					Edge{
						vertex: ((row - 1) * len(numericInput[0])) + col,
						weight: numericInput[row-1][col],
					})
			}
			// right
			if col < len(numericInput[0])-1 {
				graph[vertexIdx] = append(graph[vertexIdx],
					Edge{
						vertex: ((row) * len(numericInput[0])) + col + 1,
						weight: numericInput[row][col+1],
					})
			}
			// below
			if row < len(numericInput)-1 {
				graph[vertexIdx] = append(graph[vertexIdx],
					Edge{
						vertex: ((row + 1) * len(numericInput[0])) + col,
						weight: numericInput[row+1][col],
					})
			}
			// left
			if col > 0 {
				graph[vertexIdx] = append(graph[vertexIdx],
					Edge{
						vertex: ((row) * len(numericInput[0])) + col - 1,
						weight: numericInput[row][col-1],
					})
			}
			vertexIdx++
		}
	}
}

type loc struct {
	row int
	col int
}

func printPath(rawPath [][]int, path []int) {
	f, err := os.Create("day15/Output.html")
	if err != nil {
		fmt.Errorf("Failed to create file: %v", err)
		return
	}
	defer f.Close()
	f.WriteString(`
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="Output.css">
<link href="//fonts.googleapis.com/css?family=Source+Code+Pro:300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>
<body>
<p>Results:</p>
<pre>
<code>
`)

	// convert the list of path parts to a map for easy lookup
	pathParts := make(map[int]bool)
	for _, p := range path {
		pathParts[p] = true
	}

	toPrint := ""

	for row, str := range rawPath {
		for col, val := range str {
			if _, ok := pathParts[row*len(rawPath[0])+col]; ok {
				toPrint += "<em>" + strconv.Itoa(val) + "</em>"
			} else {
				toPrint += strconv.Itoa(val)
			}
		}
		toPrint += "\n"

	}

	f.WriteString(toPrint)

	f.WriteString(`
</pre>
</code>
</body>
</html> 
	`)
}
