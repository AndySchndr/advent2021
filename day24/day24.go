package day24

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input24.txt")
	instructions := parseInstructions(values)
	part1(instructions)
	part2(instructions)
}

type Instruction struct {
	Action int
	Reg1   int
	Reg2   int
	Val    int
}

const (
	O int = 0 // not a valid register
	W int = 1
	X int = 2
	Y int = 3
	Z int = 4
)

/*
 */
const (
	INP int = 0
	ADD int = 1
	MUL int = 2
	DIV int = 3
	MOD int = 4
	EQL int = 5
)

func part2(instructions []Instruction) {
	// Solve part 2 with a more intuitive lock-picky approach

	result := []int{}
	for i := 0; i < 14; i++ {
		// append a '1' to the result
		result = append(result, 1)

		// for each digit in the result, see what the best result seems to be
		for digit := 0; digit < len(result); digit++ {
			best := result[digit]
			bestResult, _ := execute(instructions, convertIntArrayToNum(result))
			for value := 1; value <= 9; value++ {
				result[digit] = value
				testResult, _ := execute(instructions, convertIntArrayToNum(result))
				if testResult < bestResult {
					best = value
					bestResult = testResult
				}
				fmt.Printf("Result: %2v\t\t%d\n", result, testResult)
				time.Sleep(time.Millisecond * 20)
			}
			result[digit] = best
		}
	}
	testResult, _ := execute(instructions, convertIntArrayToNum(result))
	fmt.Printf("Result: %2v\t\t%d\n", result, testResult)
}

func part1(instructions []Instruction) {
	// Parse the weird hidden args out of the actual args?
	inputs := [][]int{}
	for i, instruction := range instructions {
		if instruction.Action == INP {
			inputs = append(
				inputs,
				[]int{instructions[i+5].Val, instructions[i+15].Val},
			)
		}
	}

	inputCache := helpers.StackIntSlice{}
	result := []int{9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
	minResult := []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	for i := 0; i < 14; i++ {
		input := inputs[i]
		value := input[0]

		// There are 7 negative "Inputs" in the instructions (the number added to X)
		// which indicate that the value in register Z can be divided
		// instead of multiplied.

		// To figure out how to modify the 14 digit number to allow the division
		// to happen, we pop off the last number added to Z
		// before this negative number showed up.

		// To find the digit to affect, we start with the current index
		// of the input, and if the negative number is greater than the
		// negative of the last cached positive value (i guess??), then
		// adjust the index by the difference of the current index
		// and the index of the cached value. (kind of like going back to the
		// index of the cached value)

		// then from the result, subtract the absolute value of the sum of
		// the current value (negative) and the positive cached value
		if value > 0 {
			inputCache.Push([]int{i, input[1]})
		} else {
			cachedInput, _ := inputCache.Pop()
			cachedIndex := cachedInput[0]
			cachedValue := cachedInput[1]

			indMax, indMin := i, i
			if value > -cachedValue {
				indMax = indMax - (i - cachedIndex)
				fmt.Printf("Index: %02d Cached Index: %02d\nValue:%02d Cached Value: %02d, Result Index: %02d\n",
					i, cachedIndex, value, cachedValue, indMax)
			} else if value < -cachedValue {
				indMin = indMin - (i - cachedIndex)
			}
			result[indMax] -= helpers.AbsInt(value + cachedValue)
			minResult[indMin] += helpers.AbsInt(value + cachedValue)
		}
		fmt.Printf("Index: %02d\nInput Cache: %v\nResult: %v\n",
			i, inputCache, result)
	}
	val := convertIntArrayToNum(result)
	_, test := execute(instructions, val)
	fmt.Printf("Result of testing %v: %v\n", val, test)
	val = convertIntArrayToNum(minResult)
	_, test = execute(instructions, val)
	fmt.Printf("Result of testing %v: %v\n", val, test)
}

func convertIntArrayToNum(input []int) int64 {
	val := int64(0)
	for _, num := range input {
		val *= 10
		val += int64(num)
	}
	return val
}

func hasZero(num int64) bool {
	str := fmt.Sprintf("%d", num)
	return strings.Contains(str, "0")
}

// returns whether z is 0 at the end of execution,
// and what the value of z is at the end of execution
func execute(instructions []Instruction, input int64) (int, bool) {
	// create input stack
	InputBuffer := helpers.StackInt{}
	inputPieces := input
	for inputPieces > 0 {
		next := inputPieces % 10
		InputBuffer.Push(int(next))
		inputPieces /= 10
	}
	registers := make(map[int]int)
	registers[W] = 0
	registers[X] = 0
	registers[Y] = 0
	registers[Z] = 0
	popped := true
	for _, instruction := range instructions {
		value2 := instruction.Val
		if instruction.Reg2 != O {
			value2 = registers[instruction.Reg2]
		}
		switch instruction.Action {
		case INP:
			// inp a - Read an input value and write it to variable a.
			// fmt.Printf("Taking next input, registers: %12v Z mod 26: %d\n", registers, registers[Z]%26)
			registers[instruction.Reg1], popped = InputBuffer.Pop()
			if !popped {
				break
			}
		case ADD:
			// add a b - Add the value of a to the value of b, then store the result in variable a.
			registers[instruction.Reg1] = registers[instruction.Reg1] + value2
		case MUL:
			// mul a b - Multiply the value of a by the value of b, then store the result in variable a.
			registers[instruction.Reg1] = registers[instruction.Reg1] * value2
		case DIV:
			// div a b - Divide the value of a by the value of b, truncate the result to an integer, then store the result in variable a. (Here, "truncate" means to round the value toward zero.)
			if value2 == 0 {
				fmt.Printf("Attempted division by 0, skipping\n")
				continue
			}
			registers[instruction.Reg1] = registers[instruction.Reg1] / value2
		case MOD:
			// mod a b - Divide the value of a by the value of b, then store the remainder in variable a. (This is also called the modulo operation.)
			if value2 <= 0 {
				fmt.Printf("Attempted mod by %d, skipping\n", value2)
				continue
			}
			if registers[instruction.Reg1] < 0 {
				fmt.Printf("Attempted mod with %d, skipping\n", registers[instruction.Reg1])
				continue
			}
			registers[instruction.Reg1] = registers[instruction.Reg1] % value2
		case EQL:
			// eql a b - If the value of a and b are equal, then store the value 1 in variable a. Otherwise, store the value 0 in variable a.
			if registers[instruction.Reg1] == value2 {
				registers[instruction.Reg1] = 1
			} else {
				registers[instruction.Reg1] = 0
			}
		}
		if !popped {
			break
		}
	}
	// fmt.Printf("Done instructions, registers: %v Z mod 26: %v\n", registers, (registers[Z] % 26))

	return registers[Z], registers[Z] == 0
}

func parseInstructions(raw []string) []Instruction {
	result := make([]Instruction, len(raw))
	for i, str := range raw {
		result[i] = parseInstruction(str)
	}
	return result
}

var letterToNum = map[string]int{
	"w": W,
	"x": X,
	"y": Y,
	"z": Z,
}

func parseInstruction(raw string) Instruction {
	pieces := strings.Fields(raw)
	var instruction Instruction
	reg2 := O
	val := -1
	if len(pieces) > 2 {
		num, err := strconv.Atoi(pieces[2])
		if err != nil {
			reg2 = letterToNum[pieces[2]]
		} else {
			val = num
		}
	}
	switch pieces[0] {
	case "inp":
		instruction = Instruction{
			Action: INP,
			Reg1:   letterToNum[pieces[1]],
			Reg2:   reg2,
			Val:    val,
		}
	case "add":
		instruction = Instruction{
			Action: ADD,
			Reg1:   letterToNum[pieces[1]],
			Reg2:   reg2,
			Val:    val,
		}
	case "mul":
		instruction = Instruction{
			Action: MUL,
			Reg1:   letterToNum[pieces[1]],
			Reg2:   reg2,
			Val:    val,
		}
	case "div":
		instruction = Instruction{
			Action: DIV,
			Reg1:   letterToNum[pieces[1]],
			Reg2:   reg2,
			Val:    val,
		}
	case "mod":
		instruction = Instruction{
			Action: MOD,
			Reg1:   letterToNum[pieces[1]],
			Reg2:   reg2,
			Val:    val,
		}
	case "eql":
		instruction = Instruction{
			Action: EQL,
			Reg1:   letterToNum[pieces[1]],
			Reg2:   reg2,
			Val:    val,
		}
	}
	return instruction
}
