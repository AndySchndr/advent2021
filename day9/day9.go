package day9

import (
	"fmt"
	"sort"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input9.txt")
	lowPointValues := findLowPointValues(values)
	totalRiskLevel := computeRiskLevels(lowPointValues)
	fmt.Printf("Total low point risk levels: %d\n", totalRiskLevel)
	basinSizes := findBasinSizes(values)
	l := len(basinSizes)
	biggestProduct := basinSizes[l-1] * basinSizes[l-2] * basinSizes[l-3]
	fmt.Printf("Product of 3 biggest basins: %d\n", biggestProduct)
}

func findLowPointValues(rawInput []string) []rune {
	result := []rune{}
	for r, row := range rawInput {
		for c, char := range row {
			// check if this is a low point
			// if not the first row, check above
			if r > 0 {
				if char >= rune(rawInput[r-1][c]) {
					continue
				}
			}
			// if not the first column, check to left
			if c > 0 {
				if char >= rune(rawInput[r][c-1]) {
					continue
				}
			}
			// if not the last row, check below
			if r < len(rawInput)-1 {
				if char >= rune(rawInput[r+1][c]) {
					continue
				}
			}
			// if not the last column, check to right
			if c < len(rawInput[r])-1 {
				if char >= rune(rawInput[r][c+1]) {
					continue
				}
			}

			// passed all checks, this is a low point
			result = append(result, char)
		}
	}
	return result
}

func computeRiskLevels(values []rune) (result int) {
	for _, v := range values {
		i := int(v-'0') + 1
		result += i
	}
	return result
}

func findBasinSizes(rawInput []string) (results []int) {
	checked := make([][]int, len(rawInput))
	for i, line := range rawInput {
		checked[i] = make([]int, len(line))
	}
	for r, row := range rawInput {
		for c, char := range row {
			if checked[r][c] != 1 &&
				char != '9' {
				results = append(
					results,
					digestBasin(rawInput, checked, r, c),
				)
			}
		}
	}
	sort.Ints(results)
	return results
}

func digestBasin(rawInput []string, checked [][]int, row int, col int) int {
	size := 0
	// check if this is a valid space. If not, return
	if row < 0 || row >= len(rawInput) ||
		col < 0 || col >= len(rawInput[row]) ||
		rawInput[row][col] == '9' ||
		checked[row][col] == 1 {
		return size
	}

	// This space is valid! add 1 and set the space to invalid
	size += 1
	checked[row][col] = 1
	// search the basin in all possible directions from here
	size += digestBasin(rawInput, checked, row+1, col)
	size += digestBasin(rawInput, checked, row-1, col)
	size += digestBasin(rawInput, checked, row, col+1)
	size += digestBasin(rawInput, checked, row, col-1)

	return size
}
