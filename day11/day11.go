package day11

import (
	"fmt"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input11.txt")
	intInput := helpers.ConvertAtoIGrid(values)
	flashes := simulate(intInput, 100)
	fmt.Printf("Total flashes: %d\n", flashes)
	intInput = helpers.ConvertAtoIGrid(values)
	syncStep := simulate(intInput, 0)
	fmt.Printf("Steps to Sync: %d\n", syncStep)
}

// Kind of a crappy function, if simulations is 0, runs the simulation
// until the step that all the jellies sync up, and returns the step number.
// Otherwise returns the total number of flashes that occur after the given
// number of simulated steps.
func simulate(rawInput [][]int, simulations int) int {
	flashes := 0
	for i := 0; simulations == 0 || i < simulations; i++ {
		lastFlashedCount := 0
		for r, row := range rawInput {
			for c, _ := range row {
				if rawInput[r][c] == 0 {
					lastFlashedCount++
				}
				// for the first pass, just increment every space
				rawInput[r][c] += 1
			}
		}
		if simulations == 0 &&
			lastFlashedCount >= len(rawInput)*len(rawInput[0]) {
			return i // all octopuses will simultaneously flash this step
		}
		flashing := true
		for flashing {
			flashing = false
			for r, row := range rawInput {
				for c, val := range row {
					if val > 9 {
						// a flash!
						flashing = true
						flashes++
						rawInput[r][c] = 0
						incrementAllDirections(rawInput, r, c)
					}
				}
			}
		}
	}
	return flashes
}

func incrementAllDirections(rawInput [][]int, r int, c int) {
	// increment all possible directions, where the value is not 0
	if r > 0 {
		incrementIfNot0(rawInput, r-1, c) // 8
		if c > 0 {
			incrementIfNot0(rawInput, r-1, c-1) // 7
		}
		if c < len(rawInput[r])-1 {
			incrementIfNot0(rawInput, r-1, c+1) // 9
		}
	}
	if c > 0 {
		incrementIfNot0(rawInput, r, c-1) // 4
	}
	if c < len(rawInput[r])-1 {
		incrementIfNot0(rawInput, r, c+1) // 5
	}
	if r < len(rawInput)-1 {
		incrementIfNot0(rawInput, r+1, c) // 2
		if c > 0 {
			incrementIfNot0(rawInput, r+1, c-1) // 1
		}
		if c < len(rawInput[r])-1 {
			incrementIfNot0(rawInput, r+1, c+1) // 3
		}
	}
}

func incrementIfNot0(rawInput [][]int, row int, col int) {
	if rawInput[row][col] != 0 {
		rawInput[row][col]++
	}
}
