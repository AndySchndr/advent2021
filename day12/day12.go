package day12

import (
	"fmt"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type Graph map[string][]string

func RunDay() {
	values := helpers.ReadFileAsStrings("input12.txt")
	graph := parseGraph(values)
	paths := traverseGraph(graph)
	fmt.Printf("Total Paths: %v\n", len(paths))
	paths = traverseGraphPart2(graph)
	fmt.Printf("Total Paths Part 2: %v\n", len(paths))
}

func parseGraph(rawInput []string) Graph {
	result := make(Graph)
	for _, edge := range rawInput {
		vertices := strings.Split(edge, "-")
		result.addEdge(vertices[0], vertices[1])
	}
	return result
}

func (g *Graph) addEdge(A, B string) {
	if _, ok := (*g)[A]; !ok {
		(*g)[A] = []string{}
	}
	if !helpers.Contains((*g)[A], B) {
		(*g)[A] = append((*g)[A], B)
	}
	if _, ok := (*g)[B]; !ok {
		(*g)[B] = []string{}
	}
	if !helpers.Contains((*g)[B], A) {
		(*g)[B] = append((*g)[B], A)
	}
}

func traverseGraphPart2(g Graph) (paths [][]string) {
	// Start at "start" with a list of
	// visited vertices (empty)
	visited := []string{"start"}
	traversePathPart2(g, "start", visited, &paths)
	return
}

func traversePathPart2(
	g Graph,
	currentVertex string,
	visited []string,
	completePaths *[][]string,
) [][]string {

	if currentVertex == "end" {
		// Reached the end! return just this successful path
		result := [][]string{visited}
		return result
	}
	for _, vertex := range g[currentVertex] {
		// If this is a vertex we're allowed to visit
		// Allowed to visit IF:
		// - is not "start"
		// - is an Uppercase cave OR we HAVE NOT visited two small caves
		// - if we HAVE visited a small cave twice:
		// 		- is not a small cave we have already visited
		if vertex == "start" {
			continue
		}

		if !visitedASmallCaveTwice(visited) ||
			!helpers.IsLowerCase(vertex) ||
			!helpers.Contains(visited, vertex) {

			visitedCopy := make([]string, len(visited))
			copy(visitedCopy, visited)
			visitedCopy = append(visitedCopy, vertex)

			adventures := traversePathPart2(
				g, vertex, visitedCopy, completePaths,
			)
			*completePaths = append(
				*completePaths, adventures...,
			)
		}
	}
	return [][]string{}
}

func visitedASmallCaveTwice(visited []string) bool {
	smallCaves := make(map[string]bool)
	for _, cave := range visited {
		if cave == "start" ||
			cave == "end" {
			continue
		}
		if _, ok := smallCaves[cave]; ok {
			// got this cave again, was already in the smallCaves map
			return true
		}
		if helpers.IsLowerCase(cave) {
			smallCaves[cave] = true
		}
	}
	return false
}

func traverseGraph(g Graph) (paths [][]string) {
	// Start at "start" with a list of
	// visited vertices (empty)
	visited := []string{"start"}
	traversePath(g, "start", visited, &paths)
	return
}

func traversePath(
	g Graph,
	currentVertex string,
	visited []string,
	completePaths *[][]string,
) [][]string {

	if currentVertex == "end" {
		// Reached the end! return just this successful path
		result := [][]string{visited}
		return result
	}
	for _, vertex := range g[currentVertex] {
		// If this is a vertex we're allowed to visit
		// traverse that Path, append the adventure to completePaths,
		if !helpers.Contains(visited, vertex) || // already visited
			!helpers.IsLowerCase(vertex) { // is Uppercase, so can visit
			visitedCopy := make([]string, len(visited))
			copy(visitedCopy, visited)
			visitedCopy = append(visitedCopy, vertex)

			adventures := traversePath(
				g, vertex, visitedCopy, completePaths,
			)
			*completePaths = append(
				*completePaths, adventures...,
			)
		}
	}
	return [][]string{}
}
