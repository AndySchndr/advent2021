package day21

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input21.txt")
	player1, player2 := parsePositions(values)
	// PlayPart1(player1, player2)
	PlayPart2(player1, player2)
}

func PlayPart2(p1, p2 Player) {
	var p1Wins, p2Wins int64

	p1Wins, p2Wins = SimulateUniverse(p1, p2, p1.id, int64(0))

	fmt.Printf("Player 1 wins: %d, Player 2 wins: %d", p1Wins, p2Wins)
}

var OutcomesToUniverses = map[int]int64{
	3: 1,
	4: 3,
	5: 6,
	6: 7,
	7: 6,
	8: 3,
	9: 1,
}

func SimulateUniverse(p1, p2 Player, turn int, universes int64) (int64, int64) {
	// Add the rolled position to the current player's position
	p1Wins, p2Wins := int64(0), int64(0)
	var p1W, p2W int64
	var nextTurnPlayerID int
	if turn == p1.id {
		nextTurnPlayerID = p2.id
	} else {
		nextTurnPlayerID = p1.id
	}
	for outcome, unis := range OutcomesToUniverses {
		p1Copy := p1.Copy()
		p2Copy := p2.Copy()
		if turn == p1Copy.id {
			p1Copy.MoveDirac(outcome)
			if p1Copy.score >= 21 {
				p1Wins += unis
			} else {
				p1W, p2W = SimulateUniverse(p1Copy, p2Copy, nextTurnPlayerID, unis)
				p1Wins += p1W * unis
				p2Wins += p2W * unis
			}
		} else {
			p2Copy.MoveDirac(outcome)
			if p2Copy.score >= 21 {
				p2Wins += unis
			} else {
				p1W, p2W = SimulateUniverse(p1Copy, p2Copy, nextTurnPlayerID, unis)
				p1Wins += p1W * unis
				p2Wins += p2W * unis
			}
		}
	}
	return p1Wins, p2Wins
}

type Dice struct {
	Rolls   int
	Current int
	Max     int
}

func (d *Dice) Roll() int {
	d.Rolls++
	d.Current++
	if d.Current > d.Max {
		d.Current = 1
	}
	return d.Current
}

func (d *Dice) Roll3() int {
	return d.Roll() + d.Roll() + d.Roll()
}

type Player struct {
	id       int
	position int
	score    int
}

func (p *Player) Copy() Player {
	return Player{
		id:       p.id,
		position: p.position,
		score:    p.score,
	}
}

func (p *Player) MoveDirac(distance int) {
	newPos := (p.position + distance) % 10
	if newPos == 0 {
		newPos = 10
	}
	p.score += newPos
	p.position = newPos
}

func (p *Player) Move(d *Dice, maxSpaces int) {
	// Get the number of spaces to move
	move := d.Roll3()
	newPos := (p.position + move) % 10
	if newPos == 0 {
		newPos = 10
	}
	p.score += newPos
	fmt.Printf("Player %d is on %02d, moves %03d spaces to %02d. Score: %d\n",
		p.id,
		p.position,
		move,
		newPos,
		p.score,
	)
	p.position = newPos
}

func PlayPart1(p1, p2 Player) {
	d := Dice{
		Rolls:   0,
		Current: 0,
		Max:     100,
	}
	goal := 1000
	spacesMax := 10
	var result int
	for true {
		// Player 1
		p1.Move(&d, spacesMax)
		if p1.score >= goal {
			result = p2.score * d.Rolls
			break
		}
		// Player 2
		p2.Move(&d, spacesMax)
		if p2.score >= goal {
			result = p1.score * d.Rolls
			break
		}
	}
	fmt.Printf("Result: %d\n", result)
}

func parsePositions(raw []string) (Player, Player) {
	parts := strings.Fields(raw[0])
	p1str := parts[len(parts)-1]
	p1, _ := strconv.Atoi(p1str)

	parts = strings.Fields(raw[1])
	p2str := parts[len(parts)-1]
	p2, _ := strconv.Atoi(p2str)

	player1 := Player{
		position: p1,
		score:    0,
		id:       1,
	}
	player2 := Player{
		position: p2,
		score:    0,
		id:       2,
	}

	return player1, player2
}
