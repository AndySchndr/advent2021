package day5

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type Board struct {
	board [1000][1000]int
}
type point struct {
	x int
	y int
}

type Line struct {
	origin point
	finish point
}

func RunDay() {
	values := helpers.ReadFileAsStrings("input5.txt")
	lines := extractLines(values)
	result := drawLines(lines)
	fmt.Printf("points with multiple lines %v", result)
}

func extractLines(rawInput []string) (result []Line) {
	for _, rawLine := range rawInput {
		rawPoints := strings.Split(rawLine, " -> ")
		newLine := Line{}
		newLine.origin = extractPoint(rawPoints[0])
		newLine.finish = extractPoint(rawPoints[1])
		result = append(result, newLine)
	}
	return result
}

func extractPoint(rawPoint string) (result point) {
	xyStr := strings.Split(rawPoint, ",")
	num, err := strconv.Atoi(xyStr[0])
	if err != nil {
		log.Fatalf("extractPoint: Error converting string to int: %v %v", xyStr[0], err)
	}
	result.x = num
	num, err = strconv.Atoi(xyStr[1])
	if err != nil {
		log.Fatalf("extractPoint: Error converting string to int: %v %v", xyStr[1], err)
	}
	result.y = num
	return
}

func drawLines(lines []Line) (intersects int) {
	// assume that 1000x1000 is ok
	board := Board{}
	for _, line := range lines {
		board.drawLine(line, false)
	}
	intersects = board.countIntersects()
	return
}

func (b *Board) drawLine(line Line, straightOnly bool) {
	if straightOnly {
		if line.origin.x != line.finish.x &&
			line.origin.y != line.finish.y {
			return
		}
	}
	// We can conver horizontal and vertical by
	// essentially just moving in one direction, then the other.
	currentPoint := line.origin
	b.board[currentPoint.x][currentPoint.y] += 1
	for currentPoint.x != line.finish.x ||
		currentPoint.y != line.finish.y {
		if line.finish.x > currentPoint.x {
			currentPoint.x += 1
		} else if line.finish.x < currentPoint.x {
			currentPoint.x -= 1
		}
		if line.finish.y > currentPoint.y {
			currentPoint.y += 1
		} else if line.finish.y < currentPoint.y {
			currentPoint.y -= 1
		}
		b.board[currentPoint.x][currentPoint.y] += 1
	}
}

func (b *Board) countIntersects() (result int) {
	for x, _ := range b.board {
		for y, _ := range b.board[0] {
			if b.board[x][y] > 1 {
				result++
			}
		}
	}
	return
}
