package day22

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input22.txt")
	cubes := parseCubes(values)
	talleyCubesPart1(cubes)
	accumulateCubeVolumes(cubes, true)
	accumulateCubeVolumes(cubes, false)
}

type Pos3 struct {
	X int
	Y int
	Z int
}

type Cube struct {
	On  bool
	Min Pos3
	Max Pos3
}

func (c *Cube) Contains(pos Pos3) bool {
	if c.Min.X <= pos.X && pos.X <= c.Max.X &&
		c.Min.Y <= pos.Y && pos.Y <= c.Max.Y &&
		c.Min.Z <= pos.Z && pos.Z <= c.Max.Z {
		return true
	}
	return false
}

func (c *Cube) Volume() int64 {
	volume := int64(c.Max.X-c.Min.X+1) *
		int64(c.Max.Y-c.Min.Y+1) *
		int64(c.Max.Z-c.Min.Z+1)
	if c.On {
		return volume
	}
	return -volume
}

// Returns a cube representing the intersection of c1 and c2.
// whether the resulting cube "is on" depends on the states of
// the two cubes and whether the addition of this cube
// is considered additive or subtractive
func (c1 *Cube) Intersection(c2 Cube) (Cube, bool) {
	minX := helpers.MaxInt(c1.Min.X, c2.Min.X)
	maxX := helpers.MinInt(c1.Max.X, c2.Max.X)
	minY := helpers.MaxInt(c1.Min.Y, c2.Min.Y)
	maxY := helpers.MinInt(c1.Max.Y, c2.Max.Y)
	minZ := helpers.MaxInt(c1.Min.Z, c2.Min.Z)
	maxZ := helpers.MinInt(c1.Max.Z, c2.Max.Z)

	// for there to be an overlap, the mins and max's must make sense
	if minX > maxX || minY > maxY || minZ > maxZ {
		return Cube{}, false
	}

	var additive bool
	if c1.On && c2.On {
		// need to make this overlap subtractive so
		// the area doesn't get counted twice
		additive = false
	} else if !c1.On && !c2.On {
		// I think because of the order in which cubes are examined,
		// this area needs to be considered additive
		additive = true
	} else {
		additive = c2.On
	}

	return Cube{
		On:  additive,
		Min: Pos3{X: minX, Y: minY, Z: minZ},
		Max: Pos3{X: maxX, Y: maxY, Z: maxZ},
	}, true
}

func accumulateCubeVolumes(cubes []Cube, part1 bool) {
	finalCubes := []Cube{}

	// Keep a list of cubes which have been examined.
	// for each new cube, compare against all the existing
	// final cubes, finding intersections (cubes) which are additive
	// or subtractive to the total.
	for _, cube := range cubes {
		if part1 && helpers.AbsInt(cube.Min.X) > 50 {
			continue
		}
		newCubes := []Cube{}
		for _, finalCube := range finalCubes {
			// Find intersections
			intersection, yee := finalCube.Intersection(cube)
			if yee {
				newCubes = append(newCubes, intersection)
			}
		}

		if cube.On {
			newCubes = append(newCubes, cube)
		}

		finalCubes = append(finalCubes, newCubes...)
	}

	var volume int64
	for _, cube := range finalCubes {
		volume += cube.Volume()
	}
	if part1 {
		fmt.Printf("Part 1 final volume: %d\n", volume)
	} else {
		fmt.Printf("Part 2 final volume: %d\n", volume)
	}
}

func talleyCubesPart1(cubes []Cube) {
	var talley int64
	// part 1
	for x := -50; x <= 50; x++ {
		for y := -50; y <= 50; y++ {
			for z := -50; z <= 50; z++ {
				state := false
				pos := Pos3{X: x, Y: y, Z: z}
				for _, cube := range cubes {
					// In part 1, skip the big cubes
					if helpers.AbsInt(cube.Min.X) > 50 {
						continue
					}
					if cube.Contains(pos) {
						state = cube.On
					}
				}
				if state {
					talley++
				}
			}
		}
	}
	fmt.Printf("Part 1: %d\n", talley)

}

func parseCubes(raw []string) []Cube {
	result := []Cube{}
	for _, str := range raw {
		newCube := Cube{}
		fields := strings.Fields(str)
		if fields[0] == "on" {
			newCube.On = true
		}
		xyz := strings.Split(fields[1], ",")
		xLo, xHi := parseHiLo(xyz[0])
		yLo, yHi := parseHiLo(xyz[1])
		zLo, zHi := parseHiLo(xyz[2])
		newCube.Min = Pos3{
			X: xLo,
			Y: yLo,
			Z: zLo,
		}
		newCube.Max = Pos3{
			X: xHi,
			Y: yHi,
			Z: zHi,
		}
		result = append(result, newCube)
	}
	return result
}

// Parses the numbers out of a string like "x=-12..10"
func parseHiLo(str string) (lo, hi int) {
	str = str[2:]
	bits := strings.Split(str, "..")
	lo, _ = strconv.Atoi(bits[0])
	hi, _ = strconv.Atoi(bits[1])
	return
}
