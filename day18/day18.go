package day18

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input18.txt")
	// testReduce(values)
	// testMagnitude(values)
	part1result := part1(values)
	fmt.Printf("Part 1 result: %v\n", part1result)
	part2result := part2(values)
	fmt.Printf("Part 2 result: %v\n", part2result)
}

func testReduce(rawInput []string) {
	for _, str := range rawInput {
		fmt.Printf("Before: %s\n", str)
		result := reduce(str)
		fmt.Printf("After: %s\n", result)
	}
}

func testMagnitude(rawInput []string) {
	for _, str := range rawInput {
		result := magnitude(str)
		fmt.Printf("Magnitude: %d\n", result)
	}
}

func part1(rawInput []string) (result int) {
	num := rawInput[0]
	num = reduce(num)
	for i := 1; i < len(rawInput); i++ {
		num = addNumbers(num, rawInput[i])
		num = reduce(num)
	}
	fmt.Printf("Final Number: %s\n", num)
	result = magnitude(num)
	return result
}

func part2(rawInput []string) (result int) {
	// reduce all the numbers first, for efficiency or whatevs
	for i, str := range rawInput {
		rawInput[i] = reduce(str)
	}
	for i1, str1 := range rawInput {
		for i2, str2 := range rawInput {
			num := addNumbers(str1, str2)
			num = reduce(num)
			mag := magnitude(num)
			fmt.Printf("\n %d + %d = %d", i1, i2, mag)
			if mag > result {
				fmt.Printf("  New Max :D")
				result = mag
			}
		}
	}
	return
}

func addNumbers(left, right string) string {
	return "[" + left + "," + right + "]"
}

func reduce(snailNum string) string {
	hapen := true
	for hapen {
		snailNum, hapen = explode(snailNum)
		if hapen {
			continue
		}
		snailNum, hapen = split(snailNum)
	}
	return snailNum
}

func magnitude(snailNum string) int {
	for strings.Contains(snailNum, "[") {
		regularPairs := findRegularPairs(snailNum, true)
		for _, regularPair := range regularPairs {
			// Compute the magnitude of this pair
			mag := 3*regularPair.left + 2*regularPair.right
			magStr := strconv.Itoa(mag)
			snailNum = snailNum[:regularPair.idx] + magStr +
				snailNum[regularPair.idx+regularPair.len:]
			break
		}
	}
	result, err := strconv.Atoi(snailNum)
	if err != nil {
		fmt.Errorf("Error converting final snail num to int: %s, %v\n", snailNum, err)
	}
	return result
}

func explode(snailNum string) (string, bool) {
	regularPairs := findRegularPairs(snailNum, false)
	for _, regularPair := range regularPairs {
		nestLevel := findNestLevel(snailNum, regularPair.idx)
		if nestLevel >= 4 {
			// In the event of an explosion, the pair's left value is added
			// to the first regular number to the left of the exploding pair
			leftLenDiff := 0
			idx, leftLength, val := findClosestNumber(snailNum, 0, regularPair.idx, false)
			if idx != -1 {
				newLeftNum := regularPair.left + val
				newLeftNumStr := strconv.Itoa(newLeftNum)
				// Figure out length of new number compared to old number
				leftLenDiff = len(newLeftNumStr) - leftLength
				beforeLNum := snailNum[:idx]
				afterLNum := snailNum[idx+leftLength:]
				snailNum = beforeLNum + newLeftNumStr + afterLNum
			}
			// Replace the snail number regular pair itself with a '0'
			beforeNum := snailNum[:regularPair.idx+leftLenDiff]
			afterNum := snailNum[regularPair.idx+regularPair.len+leftLenDiff:]
			snailNum = beforeNum + "0" + afterNum
			// Same deal with the right.
			idx, rightLength, val := findClosestNumber(
				snailNum,
				regularPair.idx+1+leftLenDiff,
				len(snailNum),
				true,
			)
			if idx != -1 {
				newRightNum := regularPair.right + val
				newRightNumStr := strconv.Itoa(newRightNum)
				beforeRNum := snailNum[:idx]
				afterRNum := snailNum[idx+rightLength:]
				snailNum = beforeRNum + newRightNumStr + afterRNum
			}
			// We've exploded once, need to return and start over
			return snailNum, true
		}
	}
	return snailNum, false
}

func split(snailNum string) (string, bool) {
	// find and replace first big number
	reg := regexp.MustCompile(`(\d\d+)`)
	bigNumber := reg.FindStringIndex(snailNum)
	if bigNumber == nil {
		return snailNum, false
	}
	numStr := snailNum[bigNumber[0]:bigNumber[1]]
	num, _ := strconv.Atoi(numStr)
	leftNum := num / 2
	leftNumStr := strconv.Itoa(leftNum)
	rightNum := (num + 1) / 2
	rightNumStr := strconv.Itoa(rightNum)
	snailNum = snailNum[:bigNumber[0]] +
		"[" + leftNumStr + "," + rightNumStr + "]" +
		snailNum[bigNumber[1]:]
	return snailNum, true
}

type regularPair struct {
	// index of the left number in the whole number, as a string
	idx   int
	len   int
	left  int
	right int
}

func findRegularPairs(snailNum string, returnOne bool) (result []regularPair) {
	reg := regexp.MustCompile(`(\[[0-9]+,[0-9]+\])`)
	pairIdxs := reg.FindAllStringIndex(snailNum, -1)
	for _, pairIdx := range pairIdxs {
		numReg := regexp.MustCompile(`([0-9]+),([0-9]+)`)
		numResult := numReg.FindStringSubmatch(snailNum[pairIdx[0]:pairIdx[1]])
		left, _ := strconv.Atoi(numResult[1])
		right, _ := strconv.Atoi(numResult[2])
		result = append(result,
			regularPair{
				idx:   pairIdx[0],
				len:   pairIdx[1] - pairIdx[0],
				left:  left,
				right: right,
			})
		if returnOne {
			return
		}
	}
	return
}

func findNestLevel(snailNum string, idx int) (level int) {
	for i, char := range snailNum {
		switch char {
		case '[':
			level += 1
		case ']':
			level -= 1
		}
		if i == idx {
			break
		}
	}
	level -= 1 // you don't count the nesting of the paired number itself, i guess
	return
}

func findClosestNumber(
	snailNum string,
	min int,
	max int,
	next bool,
) (
	idx int,
	length int,
	num int,
) {
	reg := regexp.MustCompile(`([0-9]+)`)
	var substring string
	substring = snailNum[min:max]
	result := reg.FindAllStringIndex(substring, -1)
	if result == nil {
		return -1, 0, -1
	}
	resultIdx := 0
	if !next {
		resultIdx = len(result) - 1
	}
	idx = result[resultIdx][0] + min
	numStr := snailNum[result[resultIdx][0]+min : result[resultIdx][1]+min]
	length = len(numStr)
	num, _ = strconv.Atoi(numStr)
	return
}
