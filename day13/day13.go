package day13

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type fold struct {
	value int
	axis  string
}

type point struct {
	x int
	y int
}

func RunDay() {
	values := helpers.ReadFileAsStrings("input13.txt")
	grid := parsePoints(values)
	folds := parseFolds(values)
	foldThatPaper(grid, folds)
}

func parsePoints(values []string) (result []point) {
	for _, str := range values {
		if strings.Contains(str, "fold") ||
			len(str) == 0 {
			continue
		}
		vals := strings.Split(str, ",")
		x, err := strconv.Atoi(vals[0])
		if err != nil {
			fmt.Errorf("Error parsing %s to int: %v", vals[0], err)
		}
		y, err := strconv.Atoi(vals[1])
		if err != nil {
			fmt.Errorf("Error parsing %s to int: %v", vals[1], err)
		}
		pt := point{x: x, y: y}
		result = append(result, pt)
	}
	return result
}

func parseFolds(values []string) (result []fold) {
	for _, str := range values {
		if !strings.Contains(str, "fold") {
			continue
		}
		parts := strings.Fields(str)
		goods := strings.Split(parts[2], "=")

		val, err := strconv.Atoi(goods[1])
		if err != nil {
			fmt.Errorf("Error parsing %s to int: %v", goods[1], err)
		}
		newFold := fold{value: val, axis: goods[0]}
		result = append(result, newFold)
	}
	return result
}

func foldThatPaper(grid []point, folds []fold) {
	// Apply the folds to the points in the grid one at a time.
	printGrid(grid)
	for _, fold := range folds {
		for i, point := range grid {
			// change the point according to the fold
			if fold.axis == "y" {
				if point.y < fold.value {
					continue // unaffected
				}
				point.y = fold.value - (point.y - fold.value)
			} else {
				if point.x < fold.value {
					continue
				}
				point.x = fold.value - (point.x - fold.value)
			}
			grid[i] = point
		}
		printGrid(grid)
	}
}

func printGrid(grid []point) {
	biggest := point{}
	for _, point := range grid {
		if point.x > biggest.x {
			biggest.x = point.x
		}
		if point.y > biggest.y {
			biggest.y = point.y
		}
	}
	printableGrid := [][]int{}
	for y := 0; y <= biggest.y; y++ {
		row := []int{}
		for x := 0; x <= biggest.x; x++ {
			row = append(row, 0)
		}
		printableGrid = append(printableGrid, row)
	}
	points := 0
	for _, point := range grid {
		if printableGrid[point.y][point.x] == 0 {
			points++
		}
		printableGrid[point.y][point.x] = 1
	}
	fmt.Printf("Points: %d\n", points)
	for y := 0; y <= biggest.y; y++ {
		for x := 0; x <= biggest.x; x++ {
			if printableGrid[y][x] == 0 {
				fmt.Printf(".")
			} else {
				fmt.Printf("#")
			}
		}
		fmt.Println()
	}
}
