package day4

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type BingoBoard struct {
	numbers [5][5]int
	got     [5][5]int
}

func (bb *BingoBoard) printBoard() {
	fmt.Printf("Board:\n")
	for r := 0; r < 5; r++ {
		for c := 0; c < 5; c++ {
			fmt.Printf("%2d ", bb.numbers[r][c])
		}
		fmt.Printf("\n")
	}

	fmt.Printf("Got:\n")
	for r := 0; r < 5; r++ {
		for c := 0; c < 5; c++ {
			fmt.Printf("%2d ", bb.got[r][c])
		}
		fmt.Printf("\n")
	}
}

func (bb *BingoBoard) checkBoard() bool {
	for r := 0; r < 5; r++ {
		result := true
		for c := 0; c < 5; c++ {
			if bb.got[r][c] == 0 {
				result = false
				break
			}
		}
		if result {
			return result
		}
	}
	for c := 0; c < 5; c++ {
		result := true
		for r := 0; r < 5; r++ {
			if bb.got[r][c] == 0 {
				result = false
				break
			}
		}
		if result {
			return result
		}
	}
	return false
}

func (b *BingoBoard) computeBoardScore(lastNumber int) int {
	sum := 0
	for r := 0; r < 5; r++ {
		for c := 0; c < 5; c++ {
			if b.got[r][c] == 0 {
				sum += b.numbers[r][c]
			}
		}
	}
	result := sum * lastNumber
	return result
}

func (bb *BingoBoard) markBoard(number int) {
	for r := 0; r < 5; r++ {
		for c := 0; c < 5; c++ {
			if bb.numbers[r][c] == number {
				bb.got[r][c] = 1
				return
			}
		}
	}
}

func RunDay() {
	values := helpers.ReadFileAsStrings("input4.txt")
	bingoNumbers := extractBingoNumbers(values[0])
	boards := extractBoards(values)
	//winningBoard, lastNumber := runGame(bingoNumbers, boards)
	winningBoard, lastNumber := runGameToLastWinner(bingoNumbers, boards)
	winningBoard.printBoard()
	result := winningBoard.computeBoardScore(lastNumber)
	fmt.Printf("Winning board scores: %v", result)
}

func extractBingoNumbers(numbersStr string) []int {
	numberStrs := strings.Split(numbersStr, ",")
	var result []int
	for _, str := range numberStrs {
		num, err := strconv.Atoi(str)
		if err != nil {
			log.Fatalf("extractBingoNumbers: Error converting string to int: %v %v", str, err)
		}
		result = append(result, num)
	}
	return result
}

func extractBoards(rawInput []string) []BingoBoard {
	var result []BingoBoard
	for i := 1; i < len(rawInput); i++ {
		if strings.TrimSpace(rawInput[i]) == "" {
			continue
		}
		var board BingoBoard
		for r := 0; r < 5; r++ {
			rowStrs := strings.Fields(rawInput[i])
			for c, str := range rowStrs {
				num, err := strconv.Atoi(str)
				if err != nil {
					log.Fatalf("extractBoards: Error converting string to int: %v %v", str, err)
				}
				board.numbers[r][c] = num
			}
			i++
		}
		result = append(result, board)
	}
	return result
}

func runGame(numbers []int, boards []BingoBoard) (BingoBoard, int) {
	for _, num := range numbers {
		for i := 0; i < len(boards); i++ {
			boards[i].markBoard(num)
			if boards[i].checkBoard() {
				return boards[i], num
			}
		}
	}
	log.Printf("Error, no winning boards")
	return BingoBoard{}, 0
}

func runGameToLastWinner(numbers []int, boards []BingoBoard) (BingoBoard, int) {
	for _, num := range numbers {
		for i := 0; i < len(boards); i++ {
			boards[i].markBoard(num)
			if boards[i].checkBoard() {
				if len(boards) == 1 {
					// the last board has won
					return boards[i], num
				}
				// remove this board from the array, fast-ways
				boards[i] = boards[len(boards)-1]
				boards = boards[:len(boards)-1]
				i-- // back up the index by one so we don't skip a board
			}
		}
	}
	log.Printf("Error, no winning boards")
	return BingoBoard{}, 0
}
