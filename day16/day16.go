package day16

import (
	"fmt"
	"strconv"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input16easy.txt")
	challenge := helpers.ReadFileAsStrings("input16.txt")
	values = append(values, challenge...)
	for _, str := range values {
		bitStr := helpers.ParseToBitString(str)
		part1Parse(bitStr)
	}
	part2Values := helpers.ReadFileAsStrings("input16easy2.txt")
	challenge = helpers.ReadFileAsStrings("input16.txt")
	part2Values = append(part2Values, challenge...)
	for _, str := range part2Values {
		bitStr := helpers.ParseToBitString(str)
		part2Parse(bitStr)
	}
}

type Packet struct {
	Version    int
	TypeID     int
	Value      int64
	SubPackets []Packet
}

func part1Parse(rawStr string) {
	outerPacket, _ := decode(rawStr, 0)
	versionSum := sumVersions(outerPacket)
	fmt.Printf("\nVersion Total: %d", versionSum)
}

func part2Parse(rawStr string) {
	outerPacket, _ := decode(rawStr, 0)
	fmt.Printf("\nOuter packet value: %d", outerPacket.Value)

}

func decode(byteString string, i int) (Packet, int) {
	// version first 3 bits
	versionStr := byteString[i : i+3]
	version, err := strconv.ParseInt(versionStr, 2, 64)
	if err != nil {
		panic(err)
	}

	// typeID next 3 bits
	typeIDStr := byteString[i+3 : i+6]
	typeID, err := strconv.ParseInt(typeIDStr, 2, 64)
	if err != nil {
		panic(err)
	}

	i += 6
	subPackets := []Packet{}
	value := int64(0)
	if typeID == 4 {
		value, i = parseLiteral(byteString, i)
	} else {
		if byteString[i] == '0' {
			i += 1
			subPackets, i = getSubPacketsByBits(byteString, i)
		} else {
			i += 1
			subPackets, i = getSubPacketsByCount(byteString, i)
		}
		value = calculateValue(int(typeID), subPackets)
	}

	return Packet{
		Version:    int(version),
		TypeID:     int(typeID),
		Value:      value,
		SubPackets: subPackets,
	}, i

}

func sumVersions(packet Packet) int {
	total := packet.Version
	for _, subPacket := range packet.SubPackets {
		total += sumVersions(subPacket)
	}
	return total
}

func getSubPacketsByBits(byteString string, i int) ([]Packet, int) {
	subPackets := []Packet{}
	bitCount, err := strconv.ParseInt(byteString[i:i+15], 2, 64)
	if err != nil {
		panic(err)
	}
	i += 15
	count := 0
	for count < int(bitCount) {
		packets, idx := decode(byteString, i)
		subPackets = append(subPackets, packets)
		count += idx - i
		i = idx
	}
	return subPackets, i
}

func getSubPacketsByCount(byteString string, i int) ([]Packet, int) {
	subPackets := []Packet{}
	packetCount, err := strconv.ParseInt(byteString[i:i+11], 2, 32)
	if err != nil {
		panic(err)
	}
	i += 11
	for count := 0; count < int(packetCount); count++ {
		packets, idx := decode(byteString, i)
		subPackets = append(subPackets, packets)
		i = idx
	}
	return subPackets, i
}

func calculateValue(typeID int, packets []Packet) (value int64) {
	switch typeID {
	case 0:
		for _, packet := range packets {
			value += packet.Value
		}
	case 1:
		product := int64(1)
		for _, packet := range packets {
			product *= packet.Value
		}
		value = product
	case 2:
		min := packets[0].Value
		for _, packet := range packets {
			if packet.Value < min {
				min = packet.Value
			}
		}
		value = min
	case 3:
		max := packets[0].Value
		for _, packet := range packets {
			if packet.Value > max {
				max = packet.Value
			}
		}
		value = max
	case 5:
		if packets[0].Value > packets[1].Value {
			value = 1
		}
	case 6:
		if packets[0].Value < packets[1].Value {
			value = 1
		}
	case 7:
		if packets[0].Value == packets[1].Value {
			value = 1
		}
	}
	return value
}

func parseLiteral(byteString string, i int) (result int64, idx int) {
	result = 0
	// Parse 5 bits at a time until the leading bit is 0
	for true {
		theBits := byteString[i : i+5]
		i += 5
		num, err := strconv.ParseInt(theBits, 2, 64)
		if err != nil {
			panic(err)
		}
		last := num&0x10 != 0x10
		num &= 0x0F
		result = (result << 4) | num
		if last {
			break
		}
	}
	idx = i
	return
}
