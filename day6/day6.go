package day6

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input6.txt")
	fish := parseStartingFish(values)
	fishies := simulate(fish, 256)
	fmt.Printf("Fish: %v\n", fishies)
}

func parseStartingFish(rawInput []string) []int {
	rawStrs := strings.Split(rawInput[0], ",")
	result := make([]int, 9)
	for _, fishyStr := range rawStrs {
		fishyInt, err := strconv.Atoi(fishyStr)
		if err != nil {
			log.Fatalf("parseStartingFish: Error converting string to int: %v %v", fishyStr, err)
		}
		result[fishyInt]++
	}
	return result
}

func simulate(fishBuckets []int, days int) int {
	for i := 0; i < days; i++ {
		newFish := make([]int, 9)
		for b := 0; b < len(fishBuckets); b++ {
			if b == 0 {
				// For the fish at 0, move these fish to the 6 bucket,
				// and the same number to the 8 bucket (babbies)
				newFish[6] += fishBuckets[b]
				newFish[8] += fishBuckets[b]
			} else {
				newFish[b-1] += fishBuckets[b]
			}
		}
		fishBuckets = newFish
	}
	total := 0
	for _, fishies := range fishBuckets {
		total += fishies
	}
	return total
}
