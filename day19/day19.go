package day19

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type Scanner struct {
	Idx     int
	Solved  bool
	Offset  Pos3
	Beacons []Pos3
	// Vectors is a map of beacons to a list of vectors (distances) to
	// all other beacons
	Vectors         map[Pos3][]Pos3
	BeaconDistances []int
}

// copies a Scanner, does not copy beacon distances
// or Vectors
func (s *Scanner) Copy() Scanner {
	result := Scanner{
		Idx:    s.Idx,
		Solved: s.Solved,
		Offset: s.Offset,
	}
	result.Beacons = make([]Pos3, len(s.Beacons))
	copy(result.Beacons, s.Beacons)
	return result
}

func (s *Scanner) Rotate(r int) {
	for i := range s.Beacons {
		s.Beacons[i] = rotate(s.Beacons[i], r)
	}
}

func (s *Scanner) ApplyOffset(offset Pos3) {
	for i := range s.Beacons {
		s.Beacons[i].X += offset.X
		s.Beacons[i].Y += offset.Y
		s.Beacons[i].Z += offset.Z
	}
	s.Offset = offset
}

func (s *Scanner) CalculateVectors() {
	s.Vectors = map[Pos3][]Pos3{}
	for i1 := 0; i1 < len(s.Beacons); i1++ {
		for i2 := i1 + 1; i2 < len(s.Beacons); i2++ {
			s.Vectors[s.Beacons[i1]] = append(s.Vectors[s.Beacons[i1]],
				CalculateVector(s.Beacons[i1], s.Beacons[i2]),
			)
			s.Vectors[s.Beacons[i2]] = append(s.Vectors[s.Beacons[i2]],
				CalculateVector(s.Beacons[i2], s.Beacons[i1]),
			)
		}
	}
}

func CalculateVector(p1, p2 Pos3) Pos3 {
	return Pos3{
		X: p2.X - p1.X,
		Y: p2.Y - p1.Y,
		Z: p2.Z - p1.Z,
	}
}

type Pos3 struct {
	X int
	Y int
	Z int
}

func (p0 *Pos3) Equals(p1 Pos3) bool {
	return p0.X == p1.X && p0.Y == p1.Y && p0.Z == p1.Z
}

func RunDay() {
	values := helpers.ReadFileAsStrings("input19.txt")
	solve(values)
}

func solve(rawInput []string) {
	start := time.Now()
	scannerData := parseScanners(rawInput)
	calculateScannerDistances(scannerData)
	// not sure to what degree trying to find overlapping scanners initially helps
	overlaps := findOverlappingScanners(scannerData)
	result := alignScanners(scannerData, overlaps)
	duration := time.Since(start)
	fmt.Printf("Unique Beacons: %d in %v\n", len(result), duration)

	largest := findDistanceBetweenScanners(scannerData)
	fmt.Printf("Biggest distance between scanners: %d", largest)
}

func taxiDistance(p1, p2 Pos3) int {
	return helpers.AbsInt(p1.X-p2.X) +
		helpers.AbsInt(p1.Y-p2.Y) +
		helpers.AbsInt(p1.Z-p2.Z)
}

func findDistanceBetweenScanners(scanners []Scanner) (result int) {
	for i1 := 0; i1 < len(scanners); i1++ {
		for i2 := 0; i2 < len(scanners); i2++ {
			dist := taxiDistance(scanners[i1].Offset, scanners[i2].Offset)
			if dist > result {
				result = dist
			}
		}
	}
	return result
}

func calculateScannerDistances(scanners []Scanner) {
	for i, _ := range scanners {
		for i1 := 0; i1 < len(scanners[i].Beacons); i1++ {
			for i2 := i1 + 1; i2 < len(scanners[i].Beacons); i2++ {
				scanners[i].BeaconDistances = append(scanners[i].BeaconDistances,
					taxiDistance(
						scanners[i].Beacons[i1],
						scanners[i].Beacons[i2],
					),
				)
			}
		}
		sort.Ints(scanners[i].BeaconDistances)
	}
}

func findOverlappingScanners(scanners []Scanner) [][]int {
	// overlap threshold, arbitrary
	overlapThres := len(scanners[0].Beacons) * (len(scanners[0].Beacons) - 1) / 10
	result := make([][]int, 0)
	for i1 := 0; i1 < len(scanners); i1++ {
		for i2 := i1 + 1; i2 < len(scanners); i2++ {
			matches := 0
			copiedDists := make([]int, len(scanners[i2].BeaconDistances))
			copy(copiedDists, scanners[i2].BeaconDistances)
			for d1 := 0; d1 < len(scanners[i1].BeaconDistances); d1++ {
				for d2 := 0; d2 < len(scanners[i2].BeaconDistances); d2++ {
					// compare distances to all other distances. if there's
					// a match, count that and then set the copied distance
					// to -1 so we don't count twice
					if scanners[i1].BeaconDistances[d1] == copiedDists[d2] {
						matches++
						copiedDists[d2] = -1
						break
					}
				}
			}
			if matches > overlapThres {
				result = append(result,
					[]int{i1, i2},
				)
				// fmt.Printf("Scanners %d and %d overlap\n", i1, i2)
			}
		}
	}
	return result
}

func alignScanners(scanners []Scanner, overlaps [][]int) map[Pos3]struct{} {
	// Assume scanner 0 is aligned
	scanners[0].Solved = true
	scanners[0].CalculateVectors()
	// find pairs of scanners until they're all solved
	var s0, s1 int
	allSolved := false
	positionSet := make(map[Pos3]struct{})
	insertPositions(positionSet, scanners[0].Beacons)
	for !allSolved {
		allSolved = true
		for i, _ := range scanners {
			if !scanners[i].Solved {
				allSolved = false
				break
			}
		}
		for _, overlap := range overlaps {
			if scanners[overlap[0]].Solved &&
				!scanners[overlap[1]].Solved {
				s0 = overlap[0]
				s1 = overlap[1]
				break
			}
			if scanners[overlap[1]].Solved &&
				!scanners[overlap[0]].Solved {
				s0 = overlap[1]
				s1 = overlap[0]
				break
			}
		}
		scanners[s1] = solveScanners(scanners[s0], scanners[s1])
		insertPositions(positionSet, scanners[s1].Beacons)
	}
	return positionSet
}

func insertPositions(set map[Pos3]struct{}, positions []Pos3) {
	var blank struct{}
	for _, pos := range positions {
		set[pos] = blank
	}
}

// Solve scanner s1, with the assumption that s0 is aligned
// and overlaps with s1
func solveScanners(s0, s1 Scanner) Scanner {
	for r := 0; r < 24; r++ {
		copiedScanner := s1.Copy()
		copiedScanner.Rotate(r)
		copiedScanner.CalculateVectors()
		success, b1, b2 := findOverlaps(s0, copiedScanner)
		if success {
			OffSet := CalculateVector(b2, b1)
			copiedScanner.ApplyOffset(OffSet)
			copiedScanner.CalculateVectors()
			copiedScanner.Solved = true
			return copiedScanner
		}
	}
	//
	fmt.Printf("HMM, scanner %d and %d didn't overlap as expected I think\n", s0.Idx, s1.Idx)
	return s1
}

func findOverlaps(s0, s1 Scanner) (bool, Pos3, Pos3) {
	overlapThreshold := 10
	for b0 := 0; b0 < len(s0.Beacons); b0++ {
		for b1 := 0; b1 < len(s1.Beacons); b1++ {
			b0Vectors := s0.Vectors[s0.Beacons[b0]]
			b1Vectors := s1.Vectors[s1.Beacons[b1]]
			// find how many of the vectors are the same
			overlaps := 0
			for _, v0 := range b0Vectors {
				for _, v1 := range b1Vectors {
					if v0.Equals(v1) {
						overlaps++
						if overlaps > overlapThreshold {
							return true, s0.Beacons[b0], s1.Beacons[b1]
						}
					}
				}
			}
		}
	}
	return false, Pos3{}, Pos3{}
}

func parseScanners(rawInput []string) (result []Scanner) {
	var currentScanner Scanner
	for _, str := range rawInput {
		if strings.Contains(str, "scanner") {
			// make a new scanner
			currentScanner = Scanner{}
			currentScanner.Beacons = make([]Pos3, 0)
			currentScanner.BeaconDistances = make([]int, 0)
			bits := strings.Fields(str)
			scannerIdx, err := strconv.Atoi(bits[2])
			if err != nil {
				panic(err)
			}
			currentScanner.Idx = scannerIdx
		} else if len(str) == 0 {
			// this scanner is done
			result = append(result, currentScanner)
		} else {
			bits := strings.Split(str, ",")
			x, err := strconv.Atoi(bits[0])
			if err != nil {
				panic(err)
			}
			y, err := strconv.Atoi(bits[1])
			if err != nil {
				panic(err)
			}
			z, err := strconv.Atoi(bits[2])
			if err != nil {
				panic(err)
			}
			currentScanner.Beacons = append(currentScanner.Beacons,
				Pos3{
					X: x,
					Y: y,
					Z: z,
				},
			)
		}
	}
	result = append(result, currentScanner)
	return
}

func rotate(pos Pos3, idx int) Pos3 {
	// Too dum to figure out somethin else
	switch idx {
	case 0:
		pos.X, pos.Y, pos.Z = pos.X, pos.Y, pos.Z
	case 1:
		pos.X, pos.Y, pos.Z = pos.X, pos.Z, -pos.Y
	case 2:
		pos.X, pos.Y, pos.Z = pos.X, -pos.Z, pos.Y
	case 3:
		pos.X, pos.Y, pos.Z = pos.X, -pos.Y, -pos.Z

	case 4:
		pos.X, pos.Y, pos.Z = -pos.X, pos.Z, pos.Y
	case 5:
		pos.X, pos.Y, pos.Z = -pos.X, pos.Y, -pos.Z
	case 6:
		pos.X, pos.Y, pos.Z = -pos.X, -pos.Y, pos.Z
	case 7:
		pos.X, pos.Y, pos.Z = -pos.X, -pos.Z, -pos.Y

	case 8:
		pos.X, pos.Y, pos.Z = pos.Y, pos.Z, pos.X
	case 9:
		pos.X, pos.Y, pos.Z = pos.Y, pos.X, -pos.Z
	case 10:
		pos.X, pos.Y, pos.Z = pos.Y, -pos.X, pos.Z
	case 11:
		pos.X, pos.Y, pos.Z = pos.Y, -pos.Z, -pos.X

	case 12:
		pos.X, pos.Y, pos.Z = -pos.Y, pos.X, pos.Z
	case 13:
		pos.X, pos.Y, pos.Z = -pos.Y, pos.Z, -pos.X
	case 14:
		pos.X, pos.Y, pos.Z = -pos.Y, -pos.Z, pos.X
	case 15:
		pos.X, pos.Y, pos.Z = -pos.Y, -pos.X, -pos.Z

	case 16:
		pos.X, pos.Y, pos.Z = pos.Z, pos.X, pos.Y
	case 17:
		pos.X, pos.Y, pos.Z = pos.Z, pos.Y, -pos.X
	case 18:
		pos.X, pos.Y, pos.Z = pos.Z, -pos.Y, pos.X
	case 19:
		pos.X, pos.Y, pos.Z = pos.Z, -pos.X, -pos.Y

	case 20:
		pos.X, pos.Y, pos.Z = -pos.Z, pos.Y, pos.X
	case 21:
		pos.X, pos.Y, pos.Z = -pos.Z, pos.X, -pos.Y
	case 22:
		pos.X, pos.Y, pos.Z = -pos.Z, -pos.X, pos.Y
	case 23:
		pos.X, pos.Y, pos.Z = -pos.Z, -pos.Y, -pos.X
	}
	return pos
}
