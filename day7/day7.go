package day7

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input7.txt")
	positions := parseStartingPositions(values)
	result := solve(positions)
	fmt.Printf("Result: %v\n", result)
	resultPt2 := solvePt2(positions)
	fmt.Printf("Result Part 2: %v\n", resultPt2)
}

func parseStartingPositions(rawInput []string) []int {
	rawStrs := strings.Split(rawInput[0], ",")
	result := []int{}
	for _, rawStr := range rawStrs {
		rawInt, err := strconv.Atoi(rawStr)
		if err != nil {
			log.Fatalf("parseStartingPositions: Error converting string to int: %v %v", rawStr, err)
		}
		result = append(result, rawInt)
	}
	return result
}

var fuelCosts map[int]int

func solvePt2(positions []int) int {
	fuelCosts = make(map[int]int)
	fuelCosts[0] = 0
	fuelCosts[1] = 1
	sort.Ints(positions)

	// Try a binary search sort of thing?
	current := positions[len(positions)/2]
	step := (positions[len(positions)-1] - positions[0]) / 4
	csvFile, err := os.Create("values.csv")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	csvwriter := csv.NewWriter(csvFile)
	for _, pos := range positions {
		row := []string{}
		row = append(row, strconv.Itoa(pos))
		fuel := getTotalFuel(positions, pos)
		row = append(row, strconv.Itoa(fuel))
		_ = csvwriter.Write(row)
	}
	csvwriter.Flush()
	csvFile.Close()
	for true {
		currentFuel := getTotalFuel(positions, current)
		min := current - step
		max := current + step
		minFuel := getTotalFuel(positions, min)
		maxFuel := getTotalFuel(positions, max)
		fmt.Printf("Fuel at %d: %d\t", current, currentFuel)
		fmt.Printf("Fuel at %d: %d\t", min, minFuel)
		fmt.Printf("Fuel at %d: %d\n", max, maxFuel)
		if minFuel < currentFuel {
			current = min
		} else if maxFuel < currentFuel {
			current = max
		} else {
			step /= 2
		}
		if step == 0 {
			break
		}
	}

	// Compute fuel
	totalFuel := getTotalFuel(positions, current)
	return totalFuel
}

func getTotalFuel(positions []int, position int) int {
	result := 0
	for _, pos := range positions {
		dist := position - pos
		if dist < 0 {
			dist = -dist
		}
		fuel := getFuelNonLinear(dist)
		result += fuel
	}
	return result
}
func getFuelNonLinear(dist int) int {
	if fuel, ok := fuelCosts[dist]; ok {
		return fuel
	} else {
		fuelCosts[dist] = dist + getFuelNonLinear(dist-1)
		return fuelCosts[dist]
	}
}

func solve(positions []int) int {
	// Try the median?
	sort.Ints(positions)
	median := positions[len(positions)/2]
	fmt.Printf("Median: %d\n", median)

	// Compute fuel
	result := 0
	for _, pos := range positions {
		fuel := median - pos
		if fuel < 0 {
			fuel = -fuel
		}
		result += fuel
	}
	return result
}
