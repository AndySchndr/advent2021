package helpers

import (
	"container/heap"
)

// An Item is something we manage in a priority queue.
type PQItem struct {
	Value    int // The value of the item; arbitrary.
	Priority int // The priority of the item in the queue. lower is prioritized.
	// The index is needed by update and is maintained by the heap.Interface methods.
	Index int // The index of the item in the heap.
}

// A PriorityQueue implements heap.Interface and holds Items.
type PriorityQueue []*PQItem

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the lowest, not highest, priority so we use less than here.
	return pq[i].Priority < pq[j].Priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].Index = i
	pq[j].Index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*PQItem)
	item.Index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.Index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// Helper function to get an item based on value.
// kinda relies on the assumption that values are unique,
// or that you don't care
func (pq *PriorityQueue) GetItemByValue(value int) any {
	for _, item := range *pq {
		if item.Value == value {
			return item
		}
	}
	return nil
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue) Update(item *PQItem, Value int, Priority int) {
	item.Value = Value
	item.Priority = Priority
	heap.Fix(pq, item.Index)
}
