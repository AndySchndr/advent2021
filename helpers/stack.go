package helpers

// OK i KNOW a string is like an array of runes but
// please bear with me i'm trying to have fun ok
type StackRune []rune

// IsEmpty: check if stack is empty
func (s *StackRune) IsEmpty() bool {
	return len(*s) == 0
}

// Push a new value onto the stack
func (s *StackRune) Push(r rune) {
	*s = append(*s, r)
}

// Remove and return top element of stack.
// Return false if stack is empty.
func (s *StackRune) Pop() (rune, bool) {
	if s.IsEmpty() {
		return 0, false
	} else {
		index := len(*s) - 1   // Get the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}

type StackInt []int

// IsEmpty: check if stack is empty
func (s *StackInt) IsEmpty() bool {
	return len(*s) == 0
}

// Push a new value onto the stack
func (s *StackInt) Push(r int) {
	*s = append(*s, r)
}

// Remove and return top element of stack.
// Return false if stack is empty.
func (s *StackInt) Pop() (int, bool) {
	if s.IsEmpty() {
		return 0, false
	} else {
		index := len(*s) - 1   // Get the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}

type StackIntSlice [][]int

// IsEmpty: check if stack is empty
func (s *StackIntSlice) IsEmpty() bool {
	return len(*s) == 0
}

// Push a new value onto the stack
func (s *StackIntSlice) Push(r []int) {
	*s = append(*s, r)
}

// Remove and return top element of stack.
// Return false if stack is empty.
func (s *StackIntSlice) Pop() ([]int, bool) {
	if s.IsEmpty() {
		return []int{}, false
	} else {
		index := len(*s) - 1   // Get the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}
