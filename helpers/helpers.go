package helpers

import (
	"bufio"
	"fmt"
	"encoding/hex"
	"log"
	"math"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"
)

func ReadFileAsStrings(filename string) []string {
	fullFilename := getInputFile(filename)
	return readEachLine(fullFilename)
}

func ReadFileAsInts(filename string) []int {
	fullFilename := getInputFile(filename)
	lines := readEachLine(fullFilename)
	ints := make([]int, len(lines))
	for i, v := range lines {
		x, err := strconv.Atoi(v)
		if err != nil {
			log.Fatal(err)
		}
		ints[i] = x
	}
	return ints
}

// Get the name of a file relative to the current working directory.
func getInputFile(filename string) string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return path.Join(dir, filename)
}

func readEachLine(fileName string) []string {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var values []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		values = append(values, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return values
}

// Finds the average value of a group of integers.
// rounds to the *nearest* integer.
func MeanInt(vals []int) int {
	avg := MeanFloat(vals)
	result := int(math.Round(avg))
	return result
}

func MeanFloat(vals []int) float64 {
	sum := 0
	for _, val := range vals {
		sum += val
	}
	return float64(sum) / float64(len(vals))
}

func SortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return strings.Join(s, "")
}

func ContainsAllLetters(whole string, part string) bool {
	for _, char := range part {
		if !strings.ContainsRune(whole, char) {
			return false
		}
	}
	return true
}

// Converts a grid of numbers represented as a string like
// 123
// 456
// 789
// to a 2D int Array
func ConvertAtoIGrid(rawInput []string) [][]int {
	result := make([][]int, len(rawInput))
	for r, row := range rawInput {
		newRow := make([]int, len(row))
		for c, char := range row {
			newRow[c] = int(char - '0')
		}
		result[r] = newRow
	}
	return result
}

func Contains[V int | string](arr []V, needle V) bool {
	for _, v := range arr {
		if v == needle {
			return true
		}
	}
	return false
}

func IsLowerCase(s string) bool {
	return s == strings.ToLower(s)
}

func AbsInt(i int) int {
	if i < 0 { return -i }
	return i
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func ParseToBitString(str string) (result string) {
	rawBytes, err := hex.DecodeString(str)
	if err != nil {
		fmt.Errorf("Error decoding byte array from hex string: %v", err)
	}
	for _, b := range(rawBytes) {
		result += fmt.Sprintf("%08b", b)
	}
	return result
}
