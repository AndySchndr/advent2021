package day17

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

type Coords struct {
	x int
	y int
}

type Squarea struct {
	Min Coords
	Max Coords
}

func RunDay() {
	values := helpers.ReadFileAsStrings("input17.txt")
	squarea := parseTarget(values[0])
	fmt.Printf("Target:\n%d <= x <= %d\n%d <= y <= %d\n",
		squarea.Min.x,
		squarea.Max.x,
		squarea.Min.y,
		squarea.Max.y,
	)

	findGangsterVelocity(squarea)
	velocities := findGucciVelocities(squarea)
	fmt.Printf("\nSuccessful Velocities: %d", len(velocities))
}

func findGucciVelocities(area Squarea) []Coords {
	result := []Coords{}

	// minimum Y velocity is just shooting it straight there
	minYVelocity := area.Min.y
	minXVelocity := getMinXVelocity(area.Min.x)
	maxYVelocity := helpers.AbsInt(area.Min.y) - 1
	maxXVelocity := area.Max.x
	startingPos := Coords{x: 0, y: 0}
	for x := minXVelocity; x <= maxXVelocity; x++ {
		for y := minYVelocity; y <= maxYVelocity; y++ {
			// Test out every velocity pairing cuz lazy
			velocityTest := Coords{x: x, y: y}
			success, _, _ := simulateTrajectory(
				startingPos,
				velocityTest,
				area,
			)
			if success {
				fmt.Printf(" %v ", velocityTest)
				result = append(result, velocityTest)
			}
		}
		fmt.Println()
	}
	return result
}

func getMinXVelocity(xTarget int) int {
	// minimum X velocity is where you hit 0 at the target
	velocity := 0
	for xTarget > 0 {
		xTarget -= velocity
		velocity++
	}

	return velocity - 1
}

func findGangsterVelocity(area Squarea) {
	startingPos := Coords{x: 0, y: 0}
	// Because math, the Y velocity is one
	// less than the max acceptable Y position.
	// Pick an X that'll just peter out in the middle of the X range ish
	guessVelocity := Coords{
		x: getMinXVelocity(area.Min.x),
		y: helpers.AbsInt(area.Min.y) - 1,
	}
	success, _, guessMax := simulateTrajectory(startingPos, guessVelocity, area)
	if success {

		fmt.Printf("\nGuess Velocity: %v max pos: %d\n",
			guessVelocity,
			guessMax.y,
		)
	}

	return
}

func simulateTrajectory(initialPos Coords, velocity Coords, target Squarea) (bool, Coords, Coords) {
	MaxPos := Coords{x: 0, y: 0}
	currentPos := Coords{x: initialPos.x, y: initialPos.y}
	for true {
		if currentPos.x > MaxPos.x {
			MaxPos.x = currentPos.x
		}
		if currentPos.y > MaxPos.y {
			MaxPos.y = currentPos.y
		}
		if currentPos.x > target.Max.x ||
			currentPos.y < target.Min.y {
			return false, currentPos, MaxPos
		}
		if currentPos.x >= target.Min.x &&
			currentPos.x <= target.Max.x &&
			currentPos.y >= target.Min.y &&
			currentPos.y <= target.Max.y {
			return true, currentPos, MaxPos
		}
		currentPos.x += velocity.x
		currentPos.y += velocity.y
		if velocity.x > 0 {
			velocity.x -= 1
		} else if velocity.x < 0 {
			velocity.x += 1
		}
		velocity.y -= 1
	}
	return false, currentPos, MaxPos
}

func parseTarget(rawInput string) (result Squarea) {
	strs := strings.Split(rawInput, " ")
	strs[2] = strs[2][2 : len(strs[2])-1] // remove the comma
	strs[3] = strs[3][2:len(strs[3])]
	xInput := strings.Split(strs[2], "..")
	yInput := strings.Split(strs[3], "..")
	var err error
	result.Min.x, err = strconv.Atoi(xInput[0])
	if err != nil {
		panic(err)
	}
	result.Max.x, err = strconv.Atoi(xInput[1])
	if err != nil {
		panic(err)
	}
	result.Min.y, err = strconv.Atoi(yInput[0])
	if err != nil {
		panic(err)
	}
	result.Max.y, err = strconv.Atoi(yInput[1])
	if err != nil {
		panic(err)
	}
	return
}
