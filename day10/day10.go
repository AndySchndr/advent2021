package day10

import (
	"fmt"
	"sort"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input10.txt")

	corruptScores := scoreCorruptLines(values)
	fmt.Printf("Total Score: %d\n", corruptScores)
	incompleteScores := scoreIncompleteLines(values)
	medianScore := incompleteScores[len(incompleteScores)/2]
	fmt.Printf("Median incomplete line score: %d\n", medianScore)
}

func scoreCorruptLines(lines []string) (score int) {
	for _, line := range lines {
		firstInvalid := findFirstInvalid(line)
		switch firstInvalid {
		case ')':
			score += 3
		case ']':
			score += 57
		case '}':
			score += 1197
		case '>':
			score += 25137
		}
	}
	return
}

func scoreIncompleteLines(lines []string) (scores []int) {
	for _, line := range lines {
		incompleteStack, valid := getIncompleteStack(line)
		if !valid {
			continue
		}
		lineScore := 0
		token, valid := incompleteStack.Pop()
		for valid {
			switch token {
			case '(':
				lineScore = (lineScore * 5) + 1
			case '[':
				lineScore = (lineScore * 5) + 2
			case '{':
				lineScore = (lineScore * 5) + 3
			case '<':
				lineScore = (lineScore * 5) + 4
			}
			token, valid = incompleteStack.Pop()
		}
		scores = append(scores, lineScore)
	}
	sort.Ints(scores)
	return scores
}

func findFirstInvalid(line string) rune {
	checkStack := helpers.StackRune{}
	firstInvalid, checkStack := checkLine(line, checkStack, 0)
	return firstInvalid
}

func getIncompleteStack(line string) (helpers.StackRune, bool) {
	checkStack := helpers.StackRune{}
	firstInvalid, checkStack := checkLine(line, checkStack, 0)
	return checkStack, firstInvalid == 0
}

func checkLine(line string, checkStack helpers.StackRune, idx int) (rune, helpers.StackRune) {
	if idx >= len(line) {
		return 0, checkStack // line was not Corrupt I guess
	}
	switch line[idx] {
	case ')':
		expected, valid := checkStack.Pop()
		if !valid || expected != '(' {
			return rune(line[idx]), checkStack
		}
	case ']':
		expected, valid := checkStack.Pop()
		if !valid || expected != '[' {
			return rune(line[idx]), checkStack
		}
	case '}':
		expected, valid := checkStack.Pop()
		if !valid || expected != '{' {
			return rune(line[idx]), checkStack
		}
	case '>':
		expected, valid := checkStack.Pop()
		if !valid || expected != '<' {
			return rune(line[idx]), checkStack
		}
	default:
		checkStack.Push(rune(line[idx]))
	}
	return checkLine(line, checkStack, idx+1)
}
