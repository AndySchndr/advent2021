package day23

import (
	"fmt"
	"math"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input23.txt")
	rooms, hallway := parse(values)
	energy := part2(rooms, hallway)
	fmt.Printf("Done! Energy: %d\n", energy)
}

const (
	O int = 0
	A int = 1
	B int = 2
	C int = 3
	D int = 4
)

var letterMoveCost = map[int]int{
	A: 1,
	B: 10,
	C: 100,
	D: 1000,
}

type State struct {
	Rooms   [][]int
	Hallway []int
	Energy  int
}

func (s *State) Copy() State {
	// rooms
	roomsCpy := make([][]int, len(s.Rooms))
	for i := range s.Rooms {
		roomsCpy[i] = make([]int, len(s.Rooms[i]))
		copy(roomsCpy[i], s.Rooms[i])
	}
	hallCopy := make([]int, len(s.Hallway))
	copy(hallCopy, s.Hallway)
	return State{
		Rooms:   roomsCpy,
		Hallway: hallCopy,
		Energy:  s.Energy,
	}
}

type Move struct {
	srcHallway int   // source space in the hallway
	srcRoom    []int // source space in a room
	dstHallway int   // destination space in a hallway
	dstRoom    []int // destination space in a room
	letter     int
}

// Initialize with invalid position
func (m *Move) Init() {
	m.srcHallway = -1
	m.dstHallway = -1
	m.letter = O
}

func moveHallToRoom(letter int, hallSpace int, dstSpace int) Move {
	return Move{
		srcHallway: hallSpace,
		dstHallway: -1,
		dstRoom:    []int{letter - 1, dstSpace},
		letter:     letter,
	}
}

func moveRoomToRoom(letter int, srcRoom int, srcSpace int, dstSpace int) Move {
	return Move{
		srcHallway: -1,
		dstHallway: -1,
		srcRoom:    []int{srcRoom, srcSpace},
		dstRoom:    []int{letter - 1, dstSpace},
		letter:     letter,
	}
}

func moveRoomToHall(letter int, srcRoom int, srcSpace int, dstHall int) Move {
	return Move{
		srcHallway: -1,
		dstHallway: dstHall,
		srcRoom:    []int{srcRoom, srcSpace},
		letter:     letter,
	}
}

func (m *Move) InitHallway(letter int, srcHall int) {
	m.srcHallway = srcHall
	m.letter = letter
	m.dstHallway = -1
}

func (m *Move) InitRoom(letter int, srcRoom []int) {
	m.srcHallway = -1
	m.srcRoom = srcRoom
	m.letter = letter
}

// if srcHallway, use dstRoom
// if srcRoom, can be either
// srcHallway 3, dstRoom 1,3 = 4 + 1 = 5
// srcRoom 0,1 dstRoom 2,2 = 5 + 4 = 9
func (m *Move) Cost() int {
	verticalChange, horizontalChange := 0, 0
	if m.srcHallway != -1 {
		// moving from a hallway to a room
		verticalChange = m.dstRoom[1] + 1
		horizontalChange = helpers.AbsInt(m.srcHallway - ((m.dstRoom[0] * 2) + 2))
	} else {
		// moving from a room
		if m.dstHallway != -1 {
			// moving into a hallway
			verticalChange = m.srcRoom[1] + 1
			horizontalChange = helpers.AbsInt(m.dstHallway - ((m.srcRoom[0] * 2) + 2))
		} else {
			// moving into a room - the rooms really ought to be different lol
			verticalChange = m.srcRoom[1] + m.dstRoom[1] + 2
			horizontalChange = (helpers.AbsInt(m.srcRoom[0]-m.dstRoom[0]) * 2)
		}
	}
	total := verticalChange + horizontalChange
	total *= letterMoveCost[m.letter]
	return total
}

func part2(rooms [][]int, hallway []int) (energy int) {
	state := State{
		Rooms:   rooms,
		Hallway: hallway,
		Energy:  0,
	}
	lowestSuccessfulEnergy = math.MaxInt
	searchPossibleMoves(state, Move{})
	energy = lowestSuccessfulEnergy
	return
}

var lowestSuccessfulEnergy int

func searchPossibleMoves(state State, move Move) {
	// while the board has moves to do, (given, then obvious)
	movesToDo := true
	for movesToDo {
		// Execute the current move
		if move.letter != O {
			state.ExecuteMove(move)
		}
		// Check the board for success or energy failure
		if state.Energy > lowestSuccessfulEnergy {
			return
		} else if checkBoard(state.Rooms) {
			lowestSuccessfulEnergy = state.Energy
			fmt.Printf("New successful path found. Energy: %d\n", lowestSuccessfulEnergy)
			return
		}
		move, movesToDo = state.GetNextHomeMove()
	}

	// Get a list of possible moves (moving letters into the hallway)
	possibleMoves := state.GetPossibleHallMoves()
	// for each possible move, call searchPossibleMoves
	for _, move := range possibleMoves {
		newState := state.Copy()
		searchPossibleMoves(newState, move)
	}

	// If we got here, all moves were exhausted, if any.
	return
}

func (s *State) GetPossibleHallMoves() []Move {
	result := make([]Move, 0)

	// For each room, get the topmost Letter.
	// For each of these letters, add a move for each hall space it could
	// move into.
	for r := 0; r < len(s.Rooms); r++ {
		roomHomeLetter := r + 1
		makesSenseToMove := false
		var sourceSpace int
		for sourceSpace = 0; sourceSpace < len(s.Rooms[r]); sourceSpace++ {
			if s.Rooms[r][sourceSpace] != O {
				// There IS a letter here, it only makes sense to move it if
				// it, or some letter below, is not the home letter.
				for checkSpace := sourceSpace; checkSpace < len(s.Rooms[r]); checkSpace++ {
					if s.Rooms[r][checkSpace] != roomHomeLetter {
						makesSenseToMove = true
						break
					}
				}
				break
			}
		}
		if !makesSenseToMove {
			continue
		}
		letterToMove := s.Rooms[r][sourceSpace]
		// Add moves for every hallway space this guy could move into
		roomHallSpace := ((roomHomeLetter) * 2)
		// Check to the left of the room
		for h := roomHallSpace - 1; h >= 0; h-- {
			if !isValidHallPos(h) {
				continue
			} else if s.Hallway[h] == O {
				// This is a move!
				result = append(result, moveRoomToHall(letterToMove, r, sourceSpace, h))
			} else {
				break
			}
		}

		// Check to the right
		for h := roomHallSpace + 1; h < len(s.Hallway); h++ {
			if !isValidHallPos(h) {
				continue
			} else if s.Hallway[h] == O {
				// This is a move!
				result = append(result, moveRoomToHall(letterToMove, r, sourceSpace, h))
			} else {
				break
			}
		}
	}

	return result
}

func isValidHallPos(h int) bool {
	if h == 2 || h == 4 || h == 6 || h == 8 {
		return false
	}
	return true
}

func (s *State) GetNextHomeMove() (Move, bool) {
	// Check if it's possible for each room to receive letters home
	for l := A; l <= D; l++ {
		roomIdx := l - 1
		nextAvailableSpace := -1
		for space := len(s.Rooms[roomIdx]) - 1; space >= 0; space-- {
			if s.Rooms[roomIdx][space] == O {
				nextAvailableSpace = space
				break
			} else if s.Rooms[roomIdx][space] != l {
				break
			}
		}
		if nextAvailableSpace == -1 {
			continue
		}
		// this room can receive letters. are there any letters
		// in the hall that can move in?
		roomHallSpace := (l * 2)
		// Check to the left of the room
		for h := roomHallSpace - 1; h >= 0; h-- {
			if s.Hallway[h] == l {
				// This is a move!
				return moveHallToRoom(l, h, nextAvailableSpace), true
			} else if s.Hallway[h] != O {
				break
			}
		}

		// Check to the right
		for h := roomHallSpace + 1; h < len(s.Hallway); h++ {
			if s.Hallway[h] == l {
				// This is a move!
				return moveHallToRoom(l, h, nextAvailableSpace), true
			} else if s.Hallway[h] != O {
				break
			}
		}

		// Check if any of the other rooms have this letter at the top
		for r := 0; r < len(s.Rooms); r++ {
			if r+1 == l {
				continue // same room
			}
			for space := 0; space < len(s.Rooms[r]); space++ {
				if s.Rooms[r][space] == O {
					continue
				} else if s.Rooms[r][space] == l {
					// this is potentially a room!
					// Check if the hallway is clear between src and dst rooms
					clear := true
					for checkSpace := ((r + 1) * 2); checkSpace != roomHallSpace; {
						// We can assume the space outside the room is clear anyway,
						// to avoid the off-by-one error
						if checkSpace < roomHallSpace {
							checkSpace++
						} else {
							checkSpace--
						}
						if s.Hallway[checkSpace] != O {
							clear = false
							break
						}
					}
					if clear {
						return moveRoomToRoom(l, r, space, nextAvailableSpace), true
					}
				} else {
					break
				}
			}
		}
	}
	return Move{}, false
}

// Executes a move. Does not check if the move is valid
func (s *State) ExecuteMove(m Move) {
	s.Energy += m.Cost()
	if m.srcHallway != -1 {
		// moving from a hallway to a room
		s.Hallway[m.srcHallway] = O
		s.Rooms[m.dstRoom[0]][m.dstRoom[1]] = m.letter
	} else {
		// moving from a room
		s.Rooms[m.srcRoom[0]][m.srcRoom[1]] = O
		if m.dstHallway != -1 {
			// ... to a hallway
			s.Hallway[m.dstHallway] = m.letter
		} else {
			// ... to another room
			s.Rooms[m.dstRoom[0]][m.dstRoom[1]] = m.letter
		}
	}
}

func checkBoard(rooms [][]int) bool {
	for i, room := range rooms {
		letter := i + 1
		for s, _ := range room {
			if room[s] != letter {
				return false
			}
		}
	}
	return true
}

func printBoard(rooms [][]int, hall []int) {
	fmt.Printf("\n%d%d%d%d%d%d%d%d%d%d%d",
		hall[0], hall[1], hall[2], hall[3], hall[4],
		hall[5], hall[6], hall[7], hall[8], hall[9], hall[10])
	for s := 0; s < len(rooms[0]); s++ {
		fmt.Printf("\n##%d#%d#%d#%d##",
			rooms[0][s], rooms[1][s], rooms[2][s], rooms[3][s])
	}
	fmt.Println()
}

func parse(raw []string) ([][]int, []int) {
	hallway := make([]int, 11)

	rows := []string{}
	for row := 2; row < len(raw)-1; row++ {
		rowClean := strings.ReplaceAll(raw[row], "#", "")
		rows = append(rows, rowClean)
	}
	rooms := make([][]int, 4)
	for roomIdx := 0; roomIdx < 4; roomIdx++ {
		room := make([]int, len(rows))
		rooms[roomIdx] = room
	}
	for r := 0; r < len(rows); r++ {
		for c, char := range strings.TrimSpace(rows[r]) {
			switch char {
			case 'A':
				rooms[c][r] = A
			case 'B':
				rooms[c][r] = B
			case 'C':
				rooms[c][r] = C
			case 'D':
				rooms[c][r] = D
			}
		}
	}

	return rooms, hallway
}
