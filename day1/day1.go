package day1

import (
	"fmt"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsInts("input1.txt")
	increasedValues := countIncreasedValues(values)
	increasedRanges := countIncreasedRanges(values)
	fmt.Printf("Increased values: %v\n", increasedValues)
	fmt.Printf("Increased ranges: %v\n", increasedRanges)
}

func countIncreasedValues(values []int) (result int) {
	for i := 1; i < len(values); i++ {
		if values[i] > values[i-1] {
			result++
		}
	}
	return
}

func countIncreasedRanges(values []int) (result int) {
	for i := 3; i < len(values); i++ {
		if values[i]+values[i-1]+values[i-2] >
			values[i-1]+values[i-2]+values[i-3] {
			result++
		}
	}
	return
}
