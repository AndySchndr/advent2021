package day3

import (
	"fmt"
	"log"
	"strconv"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input3.txt")
	gamma, epsilon := decodeReport(values)
	fmt.Printf("Gamma:  %b\n", gamma)
	fmt.Printf("Epsilon: %b\n", epsilon)
	fmt.Printf("Product: %v\n", gamma*epsilon)
	oxygen, co2 := decodeReportPart2(values)
	fmt.Printf("oxygen:  %b\n", oxygen)
	fmt.Printf("co2: %b\n", co2)
	fmt.Printf("Product: %v\n", oxygen*co2)
}

func getIndexedCounts(values []string) []int {
	indexedCounts := make([]int, len(values[0]))
	for _, v := range values {
		for i, digit := range v {
			if digit == '1' {
				indexedCounts[i]++
			}
		}
	}
	return indexedCounts
}

func decodeReportPart2(values []string) (oxygen, co2 uint64) {
	oxygen = filterCommon(1, 0, values)
	co2 = filterCommon(0, 0, values)
	return oxygen, co2
}

func filterCommon(format int, position int, values []string) uint64 {
	if len(values) == 1 {
		result, err := strconv.ParseInt(values[0], 2, 64)
		if err != nil {
			log.Fatalf("failed converting binary str to int: %v, %v", result, err)
		}
		return uint64(result)
	}
	indexedCounts := getIndexedCounts(values)
	// Get most (or least) popular bit at the current position
	var filterValue rune
	if format == 1 {
		if float64(indexedCounts[position]) >= float64(len(values))/2 {
			filterValue = '1'
		} else {
			filterValue = '0'
		}
	} else {
		if float64(indexedCounts[position]) >= float64(len(values))/2.0 {
			filterValue = '0'
		} else {
			filterValue = '1'
		}
	}
	// log.Printf("position %d has %d/%d, and format is %d, so filtering with %v\n",
	// position, indexedCounts[position], len(values), format, filterValue)
	var filteredValues []string
	for _, value := range values {
		if value[position] == byte(filterValue) {
			filteredValues = append(filteredValues, value)
			// log.Printf("accepting %s\n", value)
		}
	}
	return filterCommon(format, position+1, filteredValues)
}

func decodeReport(values []string) (gamma, epsilon uint64) {
	indexedCounts := getIndexedCounts(values)
	var gammaStr, epsilonStr string

	for _, total := range indexedCounts {
		if total >= len(values)/2 {
			gammaStr += "1"
			epsilonStr += "0"
		} else {
			gammaStr += "0"
			epsilonStr += "1"
		}
	}
	gammaSigned, err := strconv.ParseInt(gammaStr, 2, 64)
	if err != nil {
		log.Fatalf("failed converting binary str to int: %v, %v", gammaStr, err)
	}
	gamma = uint64(gammaSigned)
	epsilon = ^gamma & 0xFFF
	return gamma, epsilon
}
