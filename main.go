package main

import (
	"gitlab.com/andyschndr/advent2021/day25"
)

func main() {
	day25.RunDay()
}
