package day8

import (
	"fmt"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input8.txt")
	counts := count1478Output(values)
	fmt.Printf("Result: %v\n", counts)
	sums := countActualOutputs(values)
	fmt.Printf("Result part 2: %v\n", sums)
}

func count1478Output(rawInput []string) int {
	result := 0
	for i, _ := range rawInput {
		rawStrs := strings.Split(rawInput[i], "|")
		outputNumsRaw := strings.Fields(rawStrs[1])
		for _, str := range outputNumsRaw {
			if len(str) == 2 ||
				len(str) == 3 ||
				len(str) == 4 ||
				len(str) == 7 {
				result++
			}
		}
	}
	return result
}

func countActualOutputs(inputs []string) int {
	result := 0
	for _, line := range inputs {
		result += decipherLine(line)
	}
	return result
}

func decipherLine(line string) int {
	signals := getSignals(line)
	outputs := getOutputs(line)
	mapping := getMapping(signals)
	outputValue := getOutput(mapping, outputs)
	return outputValue
}

func getSignals(line string) []string {
	rawStrs := strings.Split(line, "|")
	result := strings.Fields(rawStrs[0])
	for i, str := range result {
		result[i] = helpers.SortString(str)
	}
	return result
}

func getOutputs(line string) []string {
	rawStrs := strings.Split(line, "|")
	result := strings.Fields(rawStrs[1])
	for i, str := range result {
		result[i] = helpers.SortString(str)
	}
	return result
}

func getMapping(signals []string) map[string]int {
	result := make(map[string]int)
	signal1 := ""
	signal3 := ""
	var segmentF byte
	for i := 0; i < 3; i++ {
		for _, signal := range signals {
			switch len(signal) {
			case 2:
				result[signal] = 1
				signal1 = signal
			case 3:
				result[signal] = 7
			case 4:
				result[signal] = 4
			case 5:
				if i == 1 {
					if helpers.ContainsAllLetters(signal, signal1) {
						result[signal] = 3
						signal3 = signal
					}
				} else if i == 2 {
					if !helpers.ContainsAllLetters(signal, signal1) {
						if !strings.ContainsRune(signal, rune(segmentF)) {
							result[signal] = 2
						} else {
							result[signal] = 5
						}

					}
				}
			case 6:
				if i == 1 {
					if !helpers.ContainsAllLetters(signal, signal1) {
						result[signal] = 6
						if strings.ContainsRune(signal, rune(signal1[0])) {
							segmentF = signal1[0]
						} else {
							segmentF = signal1[1]
						}
					}
				} else if i == 2 {
					if helpers.ContainsAllLetters(signal, signal3) {
						result[signal] = 9
					} else if helpers.ContainsAllLetters(signal, signal1) &&
						!helpers.ContainsAllLetters(signal, signal3) {
						result[signal] = 0
					}
				}
			case 7:
				result[signal] = 8
			}
		}
	}
	//
	return result
}

func getOutput(mapping map[string]int, outputs []string) int {
	result := 0
	result += mapping[outputs[0]] * 1000
	result += mapping[outputs[1]] * 100
	result += mapping[outputs[2]] * 10
	result += mapping[outputs[3]] * 1
	return result
}
