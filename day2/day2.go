package day2

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/andyschndr/advent2021/helpers"
)

const forward string = "forward"
const down string = "down"
const up string = "up"

type Movement struct {
	direction string
	magnitude int
}

func RunDay() {
	values := helpers.ReadFileAsStrings("input2.txt")
	movements := convertToMovements(values)
	aim, depth, position := executeMovements(movements)
	fmt.Printf("Aim: %v\n", aim)
	fmt.Printf("Depth: %v\n", depth)
	fmt.Printf("position: %v\n", position)
	fmt.Printf("Product: %v\n", position*depth)
}

func convertToMovements(values []string) []Movement {
	result := make([]Movement, len(values))
	for i, v := range values {
		strs := strings.Split(v, " ")
		result[i].direction = strs[0]
		mag, err := strconv.Atoi(strs[1])
		if err != nil {
			log.Fatalf("Could not convert string to int: %v", err)
		}
		result[i].magnitude = mag
	}
	return result
}

func executeMovements(movements []Movement) (aim, depth, position int) {

	// down X increases your aim by X units.
	// up X decreases your aim by X units.
	// forward X does two things:
	// It increases your horizontal position by X units.
	// It increases your depth by your aim multiplied by X.

	for _, move := range movements {
		switch move.direction {
		case forward:
			position += move.magnitude
			depth += aim * move.magnitude
		case down:
			aim += move.magnitude
		case up:
			aim -= move.magnitude
		}
	}
	return
}
