package day20

import (
	"fmt"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input20.txt")
	encoder, grid := parseInput(values)
	runEnhancement(grid, encoder, 50)
}

func parseInput(raw []string) (string, [][]int) {
	encode := raw[0]

	result := [][]int{}

	for i := 2; i < len(raw); i++ {
		row := make([]int, len(raw[i]))
		for c, char := range raw[i] {
			if char == '#' {
				row[c] = 1
			} else {
				row[c] = 0
			}
		}
		result = append(result, row)
	}
	return encode, result
}

func addEmptySpace(grid [][]int) [][]int {
	newGrid := make([][]int, len(grid)+2)
	emptyRow := make([]int, len(grid[0])+2)
	newGrid[0] = emptyRow
	for i := 0; i < len(grid); i++ {
		newRow := make([]int, len(grid[i])+2)
		for c := 0; c < len(grid[i]); c++ {
			newRow[c+1] = grid[i][c]
		}
		newGrid[i+1] = newRow
	}
	newGrid[len(newGrid)-1] = emptyRow
	return newGrid
}

func runEnhancement(grid [][]int, encoder string, zooms int) {
	//printGrid(grid)
	gridSize := len(grid)
	for i := 0; i < 500; i++ {
		grid = addEmptySpace(grid)
	}
	for i := 0; i < zooms; i++ {
		grid = enchance(grid, encoder)
		gridSize += 2
		//printGrid(grid)
	}

	result := countLights(grid, gridSize)
	fmt.Printf("Lights: %d\n", result)
}

func enchance(grid [][]int, encoder string) [][]int {
	newGrid := make([][]int, len(grid)+2)
	for i := 0; i < len(grid)+2; i++ {
		newRow := make([]int, len(grid[0])+2)
		newGrid[i] = newRow
	}
	for r := 0; r < len(newGrid); r++ {
		for c := 0; c < len(newGrid); c++ {
			enchanceIdx := 0
			// Top left - << 8
			num := GetValue(grid, r-1, c-1)
			enchanceIdx = enchanceIdx | ((num << 8) & 0x100)

			// Top Mid - << 7
			num = GetValue(grid, r-1, c)
			enchanceIdx = enchanceIdx | ((num << 7) & 0x80)

			// Top Right - << 6
			num = GetValue(grid, r-1, c+1)
			enchanceIdx = enchanceIdx | ((num << 6) & 0x40)

			// Mid Left - << 5
			num = GetValue(grid, r, c-1)
			enchanceIdx = enchanceIdx | ((num << 5) & 0x20)

			// Mid Mid - << 4
			num = GetValue(grid, r, c)
			enchanceIdx = enchanceIdx | ((num << 4) & 0x10)

			// Mid Right - << 3
			num = GetValue(grid, r, c+1)
			enchanceIdx = enchanceIdx | ((num << 3) & 0x8)

			// Low Left - << 2
			num = GetValue(grid, r+1, c-1)
			enchanceIdx = enchanceIdx | ((num << 2) & 0x4)

			// Low Mid - << 1
			num = GetValue(grid, r+1, c)
			enchanceIdx = enchanceIdx | ((num << 1) & 0x2)

			// Low Right - << 0
			num = GetValue(grid, r+1, c+1)
			enchanceIdx = enchanceIdx | ((num << 0) & 0x1)

			if encoder[enchanceIdx] == '#' {
				newGrid[r][c] = 1
			} else {
				newGrid[r][c] = 0
			}
		}
	}

	return newGrid
}

// Helper function to get a value out of the old Grid,
// Given coordinates to the New Grid.
func GetValue(oldGrid [][]int, row int, col int) int {
	oldRow := row - 1
	oldCol := col - 1

	// Check if out of bounds
	if oldRow < 0 || oldCol < 0 ||
		oldRow > len(oldGrid)-1 ||
		oldCol > len(oldGrid[oldRow])-1 {
		return 0
	}
	return oldGrid[oldRow][oldCol]
}

func countLights(grid [][]int, gridSize int) (result int) {
	offset := (len(grid) - gridSize) / 2
	for r := offset; r < len(grid)-offset; r++ {
		for c := offset; c < len(grid[r])-offset; c++ {
			result += grid[r][c]
		}
	}
	return result
}

func printGrid(grid [][]int) {
	fmt.Printf("\nGrid:\n")
	for _, row := range grid {
		for _, col := range row {
			if col == 1 {
				fmt.Printf("#")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Printf("\n")
	}
}
