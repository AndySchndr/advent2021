package day25

import (
	"fmt"

	"gitlab.com/andyschndr/advent2021/helpers"
)

func RunDay() {
	values := helpers.ReadFileAsStrings("input25.txt")
	input := parse(values)
	steps := run(input)
	fmt.Printf("Steps: %d\n", steps)
}

const (
	EMPT int = 0
	RIGT int = 1
	DOWN int = 2
)

func run(grid [][]int) (steps int) {
	movement := true
	for movement {
		steps++
		movement = false
		// Move all the right guys to the right
		for row := 0; row < len(grid); row++ {
			firstSpaceOccupied := grid[row][0] != EMPT
			for col := 0; col < len(grid[row]); col++ {
				nextCol := col + 1
				if nextCol == len(grid[row]) {
					if firstSpaceOccupied {
						continue
					}
					nextCol = 0
				}
				if grid[row][col] == RIGT &&
					grid[row][nextCol] == EMPT {
					grid[row][nextCol], grid[row][col] = RIGT, EMPT
					movement = true
					col++ // skip the next space, we just moved there
				}
			}
		}

		// Move all the down guys down
		for col := 0; col < len(grid[0]); col++ {
			firstSpaceOccupied := grid[0][col] != EMPT
			for row := 0; row < len(grid); row++ {
				nextRow := row + 1
				if nextRow == len(grid) {
					if firstSpaceOccupied {
						continue
					}
					nextRow = 0
				}
				if grid[row][col] == DOWN &&
					grid[nextRow][col] == EMPT {
					grid[nextRow][col], grid[row][col] = DOWN, EMPT
					movement = true
					row++ // skip the next space, we just moved there
				}
			}
		}
		//fmt.Printf("After Step %d:\n", steps)
		//printGrid(grid)
	}
	return
}

func printGrid(grid [][]int) {
	for _, row := range grid {
		for _, val := range row {
			fmt.Printf("%d", val)
		}
		fmt.Printf("\n")
	}
}

func parse(raw []string) [][]int {
	result := make([][]int, len(raw))
	for i, str := range raw {
		row := make([]int, len(str))
		for c, char := range str {
			switch char {
			case '.':
				row[c] = EMPT
			case '>':
				row[c] = RIGT
			case 'v':
				row[c] = DOWN
			}
		}
		result[i] = row
	}
	return result
}
